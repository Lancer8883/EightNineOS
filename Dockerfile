FROM node:16

WORKDIR /bot

COPY package*.json ./
COPY tsconfig.json ./

COPY src ./src

RUN npm i
RUN npm run build

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

CMD [ "npm", "run", "start" ]
