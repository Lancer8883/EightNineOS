import { Guild, GuildMember } from 'discord.js';

export const getMemberById = async (guild: Guild, id: string): Promise<GuildMember | null> => {
  return guild.members.cache.get(id) ?? (await guild.members.fetch(id).catch(() => null));
};
