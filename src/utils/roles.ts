import { Guild, Role } from 'discord.js';

export const getRoleById = async (guild: Guild, id: string): Promise<Role | null> => {
  return guild.roles.cache.get(id) ?? (await guild.roles.fetch(id).catch(() => null));
};
