import { client } from '../client';
import { getConfig } from '../config/config';
import {
  CategoryChannel,
  GuildChannel,
  Snowflake,
  Permissions,
  Collection,
  VoiceChannel,
} from 'discord.js';

const config = getConfig().channels.voice.random;

client.on('voiceStateUpdate', (oldState, newState) => {
  if (newState.channel?.parentId === config.category) {
    const channels = getChannelsByVoice(newState.channelId!)?.filter((channel: GuildChannel) => {
      const isVoice = channel.type === 'GUILD_VOICE';
      const isHasPerms = !!channel
        .permissionsFor(newState.id)
        ?.has([Permissions.FLAGS.CONNECT, Permissions.FLAGS.VIEW_CHANNEL]);
      const isNotFull = !(channel as VoiceChannel).full;

      return isVoice && isHasPerms && isNotFull;
    });

    const channel = channels?.random(1)[0];

    newState.setChannel(channel?.id ?? null).catch(() => {});
  }
});

const getChannelsByVoice = (channelId: Snowflake): Collection<Snowflake, GuildChannel> | null => {
  switch (channelId) {
    case config.mod.voice:
      return getChildren(config.mod.category);
    case config.unmod.voice:
      return getChildren(config.unmod.category);
    case config.private.voice:
      return getChildren(config.private.category);
    case config.room.voice:
      return new Collection<Snowflake, GuildChannel>([
        ...(getChildren(config.mod.category) ?? []),
        ...(getChildren(config.unmod.category) ?? []),
        ...(getChildren(config.private.category) ?? []),
      ]);
    default:
      return null;
  }
};

const getChildren = (categoryID: Snowflake): Collection<Snowflake, GuildChannel> | null => {
  return (client.channels.cache.get(categoryID) as CategoryChannel)?.children;
};
