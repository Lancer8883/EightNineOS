import dotenv from 'dotenv';
dotenv.config();

import './database';
import './likeLog';
import './client';
import './commands';
import './randomVoice';
import './privateRooms';
import './unMute';
import './rolesEvents';

import { sendErrorLog } from './logs/error/error';

process.on('unhandledRejection', (reason: Error) => {
  // eslint-disable-next-line no-console
  console.log(reason);

  sendErrorLog(reason.name, reason.message);
});
