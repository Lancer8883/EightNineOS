import { CronJob } from 'cron';
import { SettingsEntity, SettingsTypes } from '../database/entities/Settings.entity';
import { client } from '../client';
import { getConfig } from '../config/config';
import { MessageEmbed, TextChannel } from 'discord.js';
import { pastasOrder } from './utils';

const sendHandler = async () => {
  const setting = await SettingsEntity.repository.findOne({
    type: SettingsTypes.LAST_PASTA,
  });

  const channel = client.channels.cache.get(getConfig().channels.text.general) as TextChannel;
  if (!channel) return;

  const dbIndex = parseInt(setting?.value ?? '0');
  const pastaText = pastasOrder[dbIndex + 1 > pastasOrder.length ? 0 : dbIndex + 1];
  const lastPastaIndex = pastasOrder.indexOf(pastaText).toString();

  if (setting) {
    setting.value = lastPastaIndex;

    setting.save();
  } else {
    const newDoc = new SettingsEntity();

    newDoc.type = SettingsTypes.LAST_PASTA;
    newDoc.value = lastPastaIndex;

    newDoc.save();
  }

  if (!pastaText) return;

  channel?.send({ embeds: [new MessageEmbed(JSON.parse(pastaText))] });
};

export const sendPastasCron = new CronJob('0 */3 * * *', sendHandler);
