import { client } from '../client';
import { getConfig } from '../config/config';
import { MessageEmbed } from 'discord.js';

client.on('messageCreate', async (message) => {
  if (message.channel.id === getConfig().channels.text.free) {
    if (message.member?.roles.cache.has(getConfig().roles.altruist)) {
      try {
        const arrayOfStrings = message.toString().split('\n');
        const embed = new MessageEmbed()
          .setTitle(arrayOfStrings[0])
          .setImage(arrayOfStrings[2])
          .setDescription(arrayOfStrings[3])
          .setFooter(arrayOfStrings[1]);
        await message.channel.send({ embeds: [embed] }).then((message) => {
          message.react(getConfig().emojis.pepeLove);
        });
      } catch (e) {}
      await message.delete();
    }
  }
});
