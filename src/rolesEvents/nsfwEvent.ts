import { client } from '../client';
import { getConfig } from '../config/config';
import { MessageEmbed, TextChannel } from 'discord.js';

client.on('messageCreate', async (message) => {
  if (message.channel.id === getConfig().channels.text.nsfwAssets) {
    if (message.member?.roles.cache.has(getConfig().roles.nsfw)) {
      try {
        const channel = client.channels.cache.get(getConfig().channels.text.nsfw) as TextChannel;
        if (!channel) return;

        await channel
          .send({
            embeds: [
              new MessageEmbed()
                .setDescription(`<@${message.member?.id}>`)
                .setImage(message.attachments.toJSON()[0]['proxyURL'])
                .setFooter(message.author.username + ' 💚'),
            ],
          })
          .then((message) => {
            message.react('💚');
          });
      } catch (e) {}
    }
  }
});
