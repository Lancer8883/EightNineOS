import { CronJob } from 'cron';
import { getRepository } from 'typeorm';
import { UsersEntity } from '../database/entities/Users.entity';
import { ReddcEntity } from '../database/entities/Reddc.entity';
import { client } from '../client';
import { getConfig } from '../config/config';
import { sendGeneral, sendModdc } from '../logs/admin/channels';
import { MessageEmbed } from 'discord.js';
import { createDescriptionTopText } from '../commands/user/tops/topVoice';

export const weekTimer = async (): Promise<void> => {
  const userRepository = getRepository(UsersEntity);
  const reddcRepository = getRepository(ReddcEntity);
  const guild = await client.guilds.fetch(getConfig().guild);

  const membersFetch = await guild.members.fetch();
  const members = membersFetch.filter(
    (member) => member.roles.cache.get(getConfig().roles.customMod) != null,
  );

  members.forEach((member) => {
    member.roles.remove(getConfig().roles.customMod);
  });

  const topMod = await UsersEntity.getTop3ByColumn('voice_time_mod', false);
  const description = createDescriptionTopText<UsersEntity>(guild, topMod, true, 'voice_time_mod');

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ 3 пользователей по времени в модерируемых каналах за неделю: ')
    .setDescription(description);

  await userRepository.createQueryBuilder().update().set({ voice_time_week: 0 }).execute();
  await userRepository.createQueryBuilder().update().set({ voice_time_mod: 0 }).execute();

  await reddcRepository
    .createQueryBuilder()
    .update()
    .set({ week_reports: 0, mutes: 0, warns: 0, bans: 0 })
    .execute();

  sendGeneral(embed);
  sendModdc(embed);
};

export const weekCron = new CronJob('30 00 * * 6', weekTimer);
