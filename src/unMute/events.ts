import { VoiceState } from 'discord.js';
import { client } from '../client';
import { getConfig } from '../config/config';

client.on('voiceStateUpdate', async (_: VoiceState, newVoice: VoiceState) => {
  const targetMember = await newVoice.guild!.members.fetch(newVoice.id);
  if (!targetMember) return;
  if (targetMember.roles.cache.has(getConfig().roles.mute)) {
    targetMember.voice.disconnect().catch(() => {});
    return;
  }

  if (
    newVoice.channel?.parent?.id === getConfig().channels.voice.modRoomCategory ||
    newVoice.channel?.parent?.id === getConfig().channels.voice.antiModRoomCategory
  )
    if (newVoice.serverMute) await newVoice.setMute(false);
});
