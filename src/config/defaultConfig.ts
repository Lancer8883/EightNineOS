export const defaultConfig = {
  prefix: '!',

  guild: '259124796971941890',

  topIgnoreUsers: [
    '331839829761589248',
    '552099153274994701',
    '259670565114085377',
    '928019289183768608',
    '567120125199777802',
  ],

  unWarnCommandUsers: [
    '331839829761589248',
    '552099153274994701',
    '254324688837148673',
    '287200686058504192',
  ],

  roles: {
    hours5000: '1049110466795937913',
    hours750: '1049109565888806913',
    passport: '1049110078566965319',

    redactor: '950814112911347746',
    headRedactor: '950815477930790985',
    moderator: '299569953768734721',
    admin: '299569797027463178',

    embed: '950844803371900948',
    png: '871073917257785406',

    mute: '950839420490231849',
    muteChat: '950842927993720892',
    guest: '436489721456099328',
    banEvents: '553616917705916416',
    banFilms: '597540975425880085',

    eventer: '950814357628006501',
    headEventer: '950816317743693904',
    altruist: '610404890472349716',
    nsfw: '646239089388945429',

    admins: [
      '950814112911347746',
      '950815477930790985',
      '299569953768734721',
      '299569797027463178',
      '289780885161639939',
      '375000541316775946',
    ],

    man: '1049104984702193685',
    woman: '1049104574629290066',

    isLocked: '869588345540444200',

    orphanage: '860266204816343091',
    loverole: '1049104421168103574',

    customMod: '876027044029403198',
  },

  webhooks: {
    dsMonitoring: '937644636384665610',
  },

  channels: {
    text: {
      general: '259124796971941890',
      duel: '376523276731809794',
      muteLog: '375800258745073667',
      warnLog: '375162612515864576',
      coinsLog: '375162577954668556',
      clanLog: '563665361078059018',
      loveLog: '886635104171065465',
      rolesLog: '825777429041512518',
      APILog: '548303243634147328',
      reportsLog: '397936119985209344',
      errorLog: '827618174538350632',
      eventsLog: '640207083446206477',
      likesLog: '937548672210718730',
      free: '694597693317840916',
      nsfw: '854619282516213780',
      nsfwAssets: '854632970827595776',
      msgsLogs: '868929483233689610',
      lottery: '259124796971941890',
      customRolesLogs: '872466039127015484',
      moddc: '538810193165942804',
      blackMarketTrust: '874308983001862184',
      blackMarket: '874324963392376853',
      voiceLog: '905855147433918595',
      joinsLog: '515040789860974594',
      removeGuest: '871848726853980210',
    },

    voice: {
      film: '609807909441896449',
      events: '725071810021949512',
      afk: '289786584247828490',
      createRoom: '589813796730699787',
      createRoomCategory: '537506491826044928',
      generalVoiceCategory: '375016246162358272',
      clanCategory: '467852401357881354',
      goodCommunication: '299595691938807811',
      badCommunication: '375023182563770368',
      modRoomCategory: '375016246162358272',
      loveRoomCategory: '873613014467747900',
      antiModRoomCategory: '748519693345882132',
      random: {
        category: '872612835220406312',
        room: {
          voice: '872613364600283136',
        },
        mod: {
          voice: '872613397462650881',
          category: '375016246162358272',
        },
        unmod: {
          voice: '872613420162252870',
          category: '748519693345882132',
        },
        private: {
          voice: '872613446733168742',
          category: '537506491826044928',
        },
      },
    },
  },

  colors: {
    success: 'GREEN',
  },

  emojis: {
    pepeLove: '<:89P_pepe_1:680009287929430028> ',
  },

  maxUserLevel: 31,
  maxEquipLevel: 30,
  commission: 0.15,
  clanPrice: 10000,
  clanExtendPrice: 2000,
  loveRoomExtendPrice: 20000,
  orphanageExtendAccrual: 2000,
  blackMarketPrice: 250,
  removeGenderPrice: 1000,

  lottery: {
    minPrice: 50,
    maxPrice: 750,
    minPlaces: 5,
    maxPlaces: 50,
  },
};
