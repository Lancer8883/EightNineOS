import { CronJob } from 'cron';
import { client } from '../client';
import { getConfig } from '../config/config';

const inviteCleanerHandler = async () => {
  const guild = client.guilds.cache.get(getConfig().guild);
  if (!guild) return;

  const invites = await guild.invites.fetch();
  const filteredInvites = invites.filter((i) => i.memberCount < 10);
  for (const [, invite] of filteredInvites) {
    await invite.delete().catch(() => {});
  }
};

export const inviteCleanerCron = new CronJob('0 0 * * *', inviteCleanerHandler);
