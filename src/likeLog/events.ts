import { client } from '../client';
import { getConfig } from '../config/config';
import { UsersEntity } from '../database/entities/Users.entity';
import { getRepository } from 'typeorm';

client.on('messageCreate', async (message) => {
  if (message.channel.id === getConfig().channels.text.likesLog) {
    try {
      if (message.embeds[0].author?.name === 'Успешно лайкнуто' || message.embeds[0].title === 'Испытайте удачу') {
        const userRepository = getRepository(UsersEntity);
        const user = await userRepository.findOne(message.embeds[0].footer!.text.slice(17));
        if (!user) return;

        user.giveCoins(300);
      }
    } catch (e) {}
  }
});
