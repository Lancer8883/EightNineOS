import { client } from './';
import { PenaltiesEntity } from '../database/entities/Penalties.entity';
import { getConfig } from '../config/config';
import { TextChannel } from 'discord.js';

client.on('guildMemberAdd', async (member) => {
  const isMuted = await PenaltiesEntity.getPenalty(member.id, true);
  if (isMuted) await member.roles.add(getConfig().roles.mute);
  const isMutedChat = await PenaltiesEntity.getPenalty(member.id, false);
  if (isMutedChat) await member.roles.add(getConfig().roles.muteChat);

  const channel = client.channels.cache.get(getConfig().channels.text.joinsLog) as TextChannel;

  const user = await client.users.cache.get(member.id);
  if (!user) return;

  channel.send(
    `🚪 ${user} зашёл на сервер. Зарегистрировался **${new Date(
      user.createdTimestamp,
    ).toLocaleDateString()}**`,
  );
});
