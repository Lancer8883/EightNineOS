import { client } from './';
import { getRepository } from 'typeorm';
import { UsersEntity } from '../database/entities/Users.entity';
import { LoveRoomsChildrenEntity } from '../database/entities/LoveRoomsChildren.entity';
import { getConfig } from '../config/config';
import { TextChannel } from 'discord.js';

client.on('guildMemberRemove', async (member) => {
  const childrenRepository = getRepository(LoveRoomsChildrenEntity);
  const userDB = await UsersEntity.getOrCreateUser(member.id);
  // @ts-ignore
  userDB.clan = null;

  userDB.save();

  const channel = client.channels.cache.get(getConfig().channels.text.joinsLog) as TextChannel;

  const user = await client.users.cache.get(member.id);
  if (!user) return;

  
  const child = await childrenRepository.findOne({
    where: [{ user_id: member.id }],
  });
  if (child) {
    await childrenRepository.delete(child);
  }

  const date = new Date(member.joinedTimestamp!);
  channel.send(
    `🚷 ${user} вышел с сервера. Ник: **${
      user.tag
    }**. Зашёл на сервер: **${date.toLocaleDateString()} ${date.toLocaleTimeString()}**`,
  );
});
