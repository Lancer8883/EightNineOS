import { GuildBan, MessageReaction, TextChannel, User } from 'discord.js';
import { client } from './';
import { getConfig } from '../config/config';

const guildBanHandler = async (ban: GuildBan) => {
  const { guild, user } = ban;

  if (guild.id !== getConfig().guild) return;

  const moddc = client.channels.cache.get(getConfig().channels.text.moddc) as TextChannel;
  const content = `<@&${
    getConfig().roles.moderator
  }>, пользователь ${user} был забанен. Очистить историю мутов?`;
  const message = await moddc.send({
    content,
  });

  await message.react('✅');
  await message.react('❌');

  const reactionColl = await message.awaitReactions({
    max: 1,
    filter: (reaction: MessageReaction, user: User) => {
      if (!reaction.emoji.name) return false;

      return ['❌', '✅'].includes(reaction.emoji.name) && !user.bot;
    },
  });

  const reaction = reactionColl.first();
  if (reaction?.emoji.name === '✅') {
    await message.edit(`Вы очистили историю мутов у ${user}.`);

    const muteLog = client.channels.cache.get(getConfig().channels.text.muteLog) as TextChannel;
    await muteLog.send(`----------${user}----------`);
  } else {
    await message.edit(`Вы отказались очищать историю мутов у ${user}.`);
  }

  await message.reactions.removeAll();
};

client.on('guildBanAdd', guildBanHandler);
