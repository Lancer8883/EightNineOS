import { Client, ClientOptions, Options } from 'discord.js';
import { inviteCleanerCron } from '../invitesCleaner/cron';
import { muteCron } from '../commands/admin/mute/muteChecker';
const ALL_INTENTS =
  (1 << 0) + // GUILDS
  (1 << 1) + // GUILD_MEMBERS
  (1 << 2) + // GUILD_BANS
  (1 << 3) + // GUILD_EMOJIS_AND_STICKERS
  (1 << 4) + // GUILD_INTEGRATIONS
  (1 << 5) + // GUILD_WEBHOOKS
  (1 << 6) + // GUILD_INVITES
  (1 << 7) + // GUILD_VOICE_STATES
  // (1 << 8) + // GUILD_PRESENCES
  (1 << 9) + // GUILD_MESSAGES
  (1 << 10) + // GUILD_MESSAGE_REACTIONS
  (1 << 11) + // GUILD_MESSAGE_TYPING
  (1 << 12) + // DIRECT_MESSAGES
  (1 << 13) + // DIRECT_MESSAGE_REACTIONS
  (1 << 14); // DIRECT_MESSAGE_TYPING

export const client = new Client({
  makeCache: Options.cacheWithLimits({
    GuildMemberManager: 1000,
  }),
  intents: ALL_INTENTS,
} as ClientOptions);

client.on('ready', () => {
  // eslint-disable-next-line no-console
  console.log('Bot ready');

  muteCron.start();
  inviteCleanerCron.start();
});

export const startBot = (): void => {
  client.login(process.env.TOKEN);
};
