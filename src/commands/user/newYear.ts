import { Message } from 'discord.js';
import { noun } from 'plural-ru';
import moment from 'moment';

export const newYear = (message: Message): void => {
  const dateNow = moment();
  const days = moment(new Date(`Dec 31 ${dateNow.year()} 23:59:59`)).diff(dateNow, 'days');

  message.channel.send(
    `:christmas_tree: До Нового Года осталось ${days} ${noun(days, 'день', 'дня', 'дней')}`,
  );
};
