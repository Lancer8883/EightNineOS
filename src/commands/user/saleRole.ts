import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { noun } from 'plural-ru';
import { getRepository } from 'typeorm';
import { ShopEntity } from '../../database/entities/Shop.entity';
import { CommandsText } from '../../logs/commands/text';
import { sendCoinsLog } from '../../logs/admin/channels';

export const saleRole = async (
  message: Message,
  user: UsersEntity,
  roleStr: string,
): Promise<void> => {
  if (!roleStr)
    throw 'https://cdn.discordapp.com/attachments/601856470450962436/903401818913116181/roles_redesign.png';

  const role = parseInt(roleStr);
  if (isNaN(role)) throw CommandsText.PARAMS_ERROR;

  const repository = getRepository(ShopEntity);
  const roleToSell = await repository.findOne({ id: role });
  if (!roleToSell) throw CommandsText.ROLE_DOES_NOT_EXIST;

  if (!message.member!.roles.cache.has(roleToSell.roleId)) throw CommandsText.USER_HAS_NOT_ROLE;

  if (roleToSell.voiceReq > user.voice_time) return;

  const embed = new MessageEmbed().setColor('BLUE').setTitle(CommandsText.ROLE_SOLD);

  user.giveCoins(roleToSell.price / 2);
  message.member!.roles.remove(roleToSell.roleId);
  message.author.send({ embeds: [embed] });

  sendCoinsLog(
    `${message.author} продал роль за ${roleToSell.price / 2} ${noun(
      roleToSell.price,
      'коин',
      'коина',
      'коинов',
    )}`,
  );
};
