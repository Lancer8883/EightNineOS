import { GuildMember, Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { RoleBoostEntity } from '../../database/entities/RoleBoost.entity';
import { noun } from 'plural-ru';
import { getRepository } from 'typeorm';
import { getConfig } from '../../config/config';
import moment from 'moment';

export const stats = async (message: Message, user: UsersEntity): Promise<void> => {
  const hours = Math.floor(user.voice_time / 3600);

  const date = moment(user.png_subscribe_timeout);
  const dateNow = moment();
  const diff = date.diff(dateNow, 'days') + 1;
  const diffRus = noun(diff, 'день', 'дня', 'дней');

  const allBoostRoles = await getRepository(RoleBoostEntity).find();
  const authorMember = await message.guild!.members.fetch(message.author.id);
  if (!authorMember) return;

  let coinsInSecond = 1;

  if (user.level > 1) coinsInSecond += (user.level - 1) * 0.2;

  // eslint-disable-next-line @typescript-eslint/naming-convention
  for (const { role_id, to_boost } of allBoostRoles) {
    if (authorMember.roles.cache.has(role_id)) coinsInSecond += to_boost;
  }

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle(`Статистика аккаунта`)
    .addField('Уровень', user.level.toString(), true)
    .addField('Коины', user.coins.toFixed(1), true)
    .addField('Время в войсе', `${hours} ${noun(hours, 'час', 'часа', 'часов')}`, true)
    .addField('Коинов в минуту', `${coinsInSecond.toFixed(1)}`, true)
    .addField('Срок подписки', user.png_subscribe_timeout ? ` ${diff} ${diffRus}` : '-', true)
    .addField('Количество варнов', `${user.warns}/3`, true);

  if (user.level == 30) embed.setFooter('Вы достигли максимального уровня');
  else embed.setFooter(`Стоимость ${user.level + 1} уровня: ${user.getNextLevelPrice()}`);

  await message.author.send({ embeds: [embed] });

  giveVoiceTimeRoles(message.member!, user.voice_time);
};

const giveVoiceTimeRoles = (member: GuildMember, voiceTime: number) => {
  if (voiceTime >= 1.8e7) {
    if (member.roles.cache.has(getConfig().roles.hours5000)) return;
    member.roles.add(getConfig().roles.hours5000);

    if (member.roles.cache.has(getConfig().roles.hours750)) {
      member.roles.remove(getConfig().roles.hours750);
    }

    if (member.roles.cache.has(getConfig().roles.passport)) {
      member.roles.remove(getConfig().roles.passport);
    }
  } else if (voiceTime >= 2.7e6 && voiceTime < 1.8e7) {
    if (member.roles.cache.has(getConfig().roles.hours750)) return;
    member.roles.add(getConfig().roles.hours750);

    if (member.roles.cache.has(getConfig().roles.passport)) {
      member.roles.remove(getConfig().roles.passport);
    }
  } else if (voiceTime >= 540000) {
    if (member.roles.cache.has(getConfig().roles.passport)) return;

    member.roles.add(getConfig().roles.passport);
  }
};
