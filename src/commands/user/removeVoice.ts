import { Message } from 'discord.js';
import { getConfig } from '../../config/config';

export const removeVoice = async (message: Message): Promise<void> => {
  const targetMember = await message.guild?.members.fetch(message.author.id);
  if (!targetMember) return;

  if (targetMember.roles.cache.has(getConfig().roles.passport)) {
    targetMember.roles.remove(getConfig().roles.passport);
  } else if (targetMember.roles.cache.has(getConfig().roles.hours750)) {
    targetMember.roles.remove(getConfig().roles.hours750);
  } else if (targetMember.roles.cache.has(getConfig().roles.hours5000)) {
    targetMember.roles.remove(getConfig().roles.hours5000);
  }
};
