import { Message, MessageEmbed } from 'discord.js';
import { getConfig } from '../../../config/config';

export const voiceLock = async (message: Message, _: null, toLockId?: string): Promise<void> => {
  const authorMember = await message.guild!.members.fetch(message.author.id);
  const voice = authorMember.voice.channel;
  if (!voice) return;

  const perm = voice.permissionOverwrites.cache
    .get(message.author.id)
    ?.allow.has('MANAGE_CHANNELS');
  if (!perm) return;

  const userToLock = message.mentions.users.first()?.id ?? toLockId;

  await voice.permissionOverwrites
    .create(userToLock ?? message.guild!.id, { CONNECT: false })
    .catch(() => null);

  await voice.permissionOverwrites
    .create(message.author!.id, { CONNECT: true, MANAGE_CHANNELS: true })
    .catch(() => null);

  const embed = new MessageEmbed()
    .setColor(getConfig().colors.success)
    .setTitle('Приватная комната')
    .setDescription(`:lock: Вы закрыли комнату от ${userToLock ? `<@${userToLock}>` : 'всех'}`);

  message.author.send({ embeds: [embed] }).catch(() => {});
};
