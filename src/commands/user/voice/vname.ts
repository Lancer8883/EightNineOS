import { Message, MessageEmbed } from 'discord.js';
import { getConfig } from '../../../config/config';

export const voiceSetName = async (
  message: Message,
  _: null,
  ...nameArr: string[]
): Promise<void> => {
  const authorMember = await message.guild!.members.fetch(message.author.id);
  const voice = authorMember.voice.channel;
  if (!voice) return;

  const { author } = message;

  const perm = voice.permissionOverwrites.cache.get(author.id)?.allow.has('MANAGE_CHANNELS');
  if (!perm) return;

  const name = nameArr.join(' ');

  if (!name || name.length >= 25) throw `Укажите имя канала (до 25 символов)`;

  await voice.setName(name).catch(() => null);

  const embed = new MessageEmbed()
    .setColor(getConfig().colors.success)
    .setTitle('Приватная комната')
    .setDescription(`:name_badge: Вы установили новое название канала: ${name}`);

  message.author.send({ embeds: [embed] }).catch(() => null);
};
