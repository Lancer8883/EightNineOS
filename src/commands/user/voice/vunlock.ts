import { Message, MessageEmbed } from 'discord.js';
import { getConfig } from '../../../config/config';

export const voiceUnLock = async (
  message: Message,
  _: null,
  toUnlockId?: string,
): Promise<void> => {
  const authorMember = await message.guild!.members.fetch(message.author.id);
  const voice = authorMember.voice.channel;
  if (!voice) return;

  const { author, guild, mentions } = message;

  const perm = voice.permissionOverwrites.cache.get(author.id)?.allow.has('MANAGE_CHANNELS');
  if (!perm) return;

  const userToUnLock = mentions.users.first()?.id ?? toUnlockId;

  await voice.permissionOverwrites
    .create(userToUnLock ?? guild!.id, { CONNECT: true })
    .catch(() => null);

  const embed = new MessageEmbed()
    .setColor(getConfig().colors.success)
    .setTitle('Приватная комната')
    .setDescription(
      `:unlock: Вы открыли комнату для ${userToUnLock ? ` <@${userToUnLock}>` : 'всех'}`,
    );

  message.author.send({ embeds: [embed] }).catch(() => {});
};
