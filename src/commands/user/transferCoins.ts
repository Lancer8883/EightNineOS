import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { CommandsText } from '../../logs/commands/text';
import { sendCoinsLog } from '../../logs/admin/channels';
import { noun } from 'plural-ru';
import { getConfig } from '../../config/config';

export const transferCoins = async (
  message: Message,
  user: UsersEntity,
  _: string,
  strSum: string,
): Promise<void> => {
  const target = message.mentions.users.first();

  const sum = parseInt(strSum);
  const sumRus = noun(sum, 'коин', 'коина', 'коинов');
  if (isNaN(sum) || !target || target.id === message.author.id) throw CommandsText.PARAMS_ERROR;

  if (sum < 50) throw CommandsText.TRANSFER_MIN;

  const commission = getConfig().commission * sum;
  const neededCoins = commission + sum;

  if (user.coins < neededCoins) {
    const enoughCoins = neededCoins - user.coins;
    const commissionRus = noun(commission, 'коин', 'коина', 'коинов');
    const enoughCoinsRus = noun(enoughCoins, 'коин', 'коина', 'коинов');
    const userCoins = noun(user.coins, 'коин', 'коина', 'коинов');

    throw new MessageEmbed()
      .setColor('RED')
      .setTitle(`Недостаточно коинов`)
      .addField('Не хватает', enoughCoins.toFixed(1) + ' ' + enoughCoinsRus, true)
      .addField('Сумма перевода', sum + ' ' + sumRus, true)
      .addField('Комиссия', commission.toFixed(1) + ' ' + commissionRus, true)
      .setFooter('Баланс: ' + user.coins + ' ' + userCoins);
  }

  const targetUser = await UsersEntity.getOrCreateUser(target.id);

  user.takeCoins(neededCoins);
  targetUser.giveCoins(sum);
  target.send(`${message.author} перевел вам **${sum}** ${sumRus}`);
  sendCoinsLog(`Пользователь ${message.author} перевел ${sum} ${sumRus} пользователю ${target}`);
};
