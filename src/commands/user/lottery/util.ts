import { client } from '../../../client';
import { getConfig } from '../../../config/config';
import { Message, MessageEmbed, MessageEmbedOptions, TextChannel } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { LotteryEntity } from '../../../database/entities/Lottery.entity';
import { noun } from 'plural-ru';
import _ from 'lodash';

const lotteryConfig = getConfig().lottery;
let nextLottery: { price: number; maxTickets: number } | null;
let message: Message;

const init = async (): Promise<void> => {
  const activeLottery = await LotteryEntity.getActiveLottery();
  if (activeLottery) {
    const channel = (await client.channels.fetch(getConfig().channels.text.lottery)) as TextChannel;
    message = await channel.messages.fetch(activeLottery.message_id);
  } else {
    createLottery();
  }
};

export const getLottery = async (): Promise<LotteryEntity | undefined> => {
  return LotteryEntity.getActiveLottery();
};

export const getAvailableTickets = (lotteryEntity: LotteryEntity): number => {
  return lotteryEntity.maxTickets - getTicketsCount(lotteryEntity);
};

export const setLottery = (maxTickets: number, price: number): void => {
  nextLottery = {
    price,
    maxTickets,
  };
};

export const buyTickets = async (
  user_id: string,
  count: number,
  lotteryEntity: LotteryEntity,
): Promise<void> => {
  await lotteryEntity.buyTickets(user_id, count);

  getTicketsCount(lotteryEntity) >= lotteryEntity.maxTickets
    ? await chooseWinner()
    : await editTemplate(lotteryEntity);
};

const getPrize = (tickets: number, lotteryEntity: LotteryEntity): number => {
  return Math.floor(lotteryEntity.price * tickets * 0.7);
};

const getTicketsCount = (lotteryEntity: LotteryEntity): number => {
  return _.sum(lotteryEntity.userTickets.map((t) => t.count));
};

const createLottery = async (): Promise<void> => {
  message = await initMessage();
  const params = nextLottery || {
    price: _.random(lotteryConfig.minPrice, lotteryConfig.maxPrice),
    maxTickets: _.random(lotteryConfig.minPlaces, lotteryConfig.maxPlaces),
  };

  const lotteryEntity = await LotteryEntity.createLottery(
    params.price,
    params.maxTickets,
    message.id,
  );

  nextLottery = null;

  await editTemplate(lotteryEntity);
};

const chooseWinner = async (): Promise<void> => {
  const lotteryEntity = await LotteryEntity.getActiveLottery();
  if (!lotteryEntity) return;

  const luckyTicket = _.random(1, getTicketsCount(lotteryEntity));
  let userMaxTicket = 0;

  for (const userTicket of lotteryEntity.userTickets) {
    userMaxTicket += userTicket.count;
    if (userMaxTicket >= luckyTicket) {
      await lotteryEntity.setWinner(userTicket.user_id);

      const prize = getPrize(getTicketsCount(lotteryEntity), lotteryEntity);
      message.channel.send(
        `<@${userTicket.user_id}> выиграл ${prize} ${noun(prize, 'коин', 'коина', 'коинов')}!`,
      );

      const winner = await UsersEntity.getOrCreateUser(userTicket.user_id);
      await winner.giveCoins(prize);

      await editTemplate(lotteryEntity);
      message.unpin();

      await lotteryEntity.delete();
      await createLottery();
      break;
    }
  }
};

const initMessage = async (): Promise<Message> => {
  const channel = (await client.channels.fetch(getConfig().channels.text.lottery)) as TextChannel;
  const msg = await channel.send({ embeds: [{ title: 'Loading...' }] });

  return msg.pin();
};

const editTemplate = async (lotteryEntity: LotteryEntity): Promise<void> => {
  await message.edit({
    embeds: [getTemplate(lotteryEntity)],
  });
};

const getTemplate = (lotteryEntity: LotteryEntity): MessageEmbed | MessageEmbedOptions => {
  let description = `
    **Занятость лотов**: ${getTicketsCount(lotteryEntity)}/${lotteryEntity.maxTickets}
    **Стоимость лота**: ${lotteryEntity.price}
    **Выигрыш на данный момент:**: ${getPrize(getTicketsCount(lotteryEntity), lotteryEntity)}
    **Главный выигрыш**: ${getPrize(lotteryEntity.maxTickets, lotteryEntity)}
    `;

  if (lotteryEntity.winner) {
    description += `\n**__Победитель__**: <@${lotteryEntity.winner}>`;
  }

  return {
    title: '**__Лотерея__**',
    description,
    color: 3553599,
    footer: {
      text: '!buyticket [кол-во билетов]',
      iconURL:
        'https://cdn.discordapp.com/attachments/663499533057130498/855508018383552572/ticket.png',
    },
  };
};

client.on('ready', init);
