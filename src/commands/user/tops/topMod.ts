import { Message, MessageEmbed } from 'discord.js';
import { createDescriptionTopText } from './topVoice';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const topMod = async (message: Message): Promise<void> => {
  const guild = message.guild;
  if (!guild) return;
  await message.guild?.channels.fetch();
  const topMod = await UsersEntity.getTopByColumn('voice_time_mod', false);
  const description = createDescriptionTopText<UsersEntity>(guild, topMod, false);

  const embed = new MessageEmbed()
    .setColor('#386AFF')
    .setTitle('Топ пользователей по времени в модерируемых каналах за неделю: ')
    .setDescription(description);

  message.channel.send({ embeds: [embed] }).then((msg) => {
    setTimeout(function () {
      msg.delete();
    }, 30000);
  });
};
