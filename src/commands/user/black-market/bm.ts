import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { getRepository } from 'typeorm';
import { BlackMarketEntity } from '../../../database/entities/BlackMarket.entity';
import { getConfig } from '../../../config/config';
import { CommandsText } from '../../../logs/commands/text';
import { isNaN } from 'lodash';
import './events';

export const bm = async (
  message: Message,
  user: UsersEntity,
  coins: string,
  ...description: string[]
): Promise<void> => {
  if (!coins || !description || isNaN(Number(coins))) throw CommandsText.PARAMS_ERROR;
  if (user.coins < 250) throw CommandsText.NO_COINS;

  const repository = getRepository(BlackMarketEntity);
  const channel = message.guild!.channels.cache.get(
    getConfig().channels.text.blackMarketTrust,
  )! as TextChannel;

  const embed = new MessageEmbed()
    .setColor('#2F3136')
    .setDescription(
      `**Пользователь**: ${
        message.author
      }\n\n**Количество коинов**: ${coins}\n\n**Описание услуги**: ${description.join(' ')}`,
    );

  const sentMessage = await channel.send({ embeds: [embed] });

  message.author.send({
    embeds: [new MessageEmbed().setColor('BLUE').setTitle(CommandsText.SUCCESSFULL_ADV)],
  });

  const market = await repository.create({
    message_id: sentMessage.id,
    user_id: message.author.id,
    decided: false,
  });

  await repository.save(market);

  sentMessage.react('✔');
  sentMessage.react('❌');
};
