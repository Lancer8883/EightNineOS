import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';
import { getRepository } from 'typeorm';
import { BlackMarketEntity } from '../../../database/entities/BlackMarket.entity';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { getConfig } from '../../../config/config';

export const bmcancel = async (
  message: Message,
  user: UsersEntity,
  messageID: string,
): Promise<void> => {
  if (!messageID) throw CommandsText.PARAMS_ERROR;

  const blackMarketRepository = getRepository(BlackMarketEntity);
  const row = await blackMarketRepository.findOne({
    where: { message_id: messageID, user_id: message.author.id },
  });
  if (!row) return;

  const channel = message.guild!.channels.cache.get(
    getConfig().channels.text.blackMarket,
  ) as TextChannel;
  if (!channel) return;

  await channel.messages.delete(messageID);
  await blackMarketRepository.delete(row);

  message.author.send({
    embeds: [new MessageEmbed().setColor('BLUE').setTitle(CommandsText.SUCCESSFULL_ADV_DELETE)],
  });
};
