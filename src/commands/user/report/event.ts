import { client } from '../../../client';
import { reports } from './report';
import { ReddcEntity } from '../../../database/entities/Reddc.entity';
import { MessageEmbed, TextChannel, User } from 'discord.js';
import { getConfig } from '../../../config/config';

client.on('messageReactionAdd', async (react, user) => {
  if (user.bot || react.message.channel.type !== 'DM') return;

  const reportID = react.message.embeds[0]?.footer?.text;
  if (!reportID) return;

  const reportsList = reports.get(reportID);
  if (!reportsList) return;
  reports.delete(reportID);

  for (const report of reportsList) {
    const channel = client.channels.cache.get(report.channelId) as TextChannel;
    const message = channel?.messages.cache.get(report.messageID);
    if (!message) continue;

    const newReportEmbed = new MessageEmbed()
      .setColor('#e01b2b')
      .setDescription(message.embeds[0].description + `\n*Репорт взял ${user}*`)
      .setFooter(message.embeds[0].footer!.text ?? '', message.embeds[0].footer!.iconURL)
      .setTimestamp();

    message.edit({ embeds: [newReportEmbed] });
  }
  const guild = client.guilds.cache.get(getConfig().guild)!;
  const targetMember = guild.members.cache.get(reportID);
  const reportTakenBy = new MessageEmbed()
    .setColor('BLUE')
    .setTitle('Вашу жалобу рассматривает ' + user.tag);
  targetMember?.send({ embeds: [reportTakenBy] });

  if (targetMember?.voice.channel) {
    const voiceInvite = await targetMember.voice.channel.createInvite();
    user.send(voiceInvite.url);

    const member = guild.members.cache.get(user.id);
    if (member?.voice.channel) {
      member.voice.setChannel(targetMember.voice.channel?.id);
    }
  }

  ReddcEntity.addStat(user as User, { reports: 1, week_reports: 1 });
});
