import { CronJob } from 'cron';
import { getRepository } from 'typeorm';
import { PenaltiesEntity } from '../../../database/entities/Penalties.entity';
import { client } from '../../../client';
import { getConfig } from '../../../config/config';
import moment from 'moment';
import { MessageEmbed } from 'discord.js';
import { getRoleById } from '../../../utils/roles';

const checker = async () => {
  const repository = getRepository(PenaltiesEntity);
  const guild = client.guilds.cache.get(getConfig().guild);
  const currentTime = moment();
  const config = getConfig();
  const penalties = await repository.find();
  const roleMute = await getRoleById(guild!, config.roles.mute);
  const roleMuteChat = await getRoleById(guild!, config.roles.muteChat);

  if (!guild || !roleMute || !roleMuteChat) return;

  for await (const penalty of penalties) {
    const isPenaltyExpired = currentTime.isAfter(penalty.date);
    if (!isPenaltyExpired) continue;

    try {
      const member = await guild.members.fetch(penalty.user_id);
      if (member) {
        await member.roles.remove(penalty.isMuted ? roleMute : roleMuteChat);
        await member.send({
          embeds: [new MessageEmbed().setColor('BLUE').setTitle('Срок мута истёк')],
        });
      }
    } catch {}

    await repository.remove(penalty);
  }
};

export const muteCron = new CronJob('* * * * *', checker);
