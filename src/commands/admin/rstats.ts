import { Message, MessageAttachment } from 'discord.js';
import { ReddcEntity } from '../../database/entities/Reddc.entity';
import { getRepository } from 'typeorm';
import { getConfig } from '../../config/config';
import AsciiTable from 'ascii-table';

export const rstats = async (message: Message): Promise<void> => {
  const adminUsersRepo = await getRepository(ReddcEntity);

  const table = new AsciiTable('Статистика редакторов');
  table.setHeading(
    'Редактор',
    'Репорты',
    'Репорты за неделю',
    'Муты за неделю',
    'Варны за неделю',
    'Баны за неделю',
  );

  const redccRole = await message.guild?.roles.fetch(getConfig().roles.redactor);
  if (!redccRole) return;

  const redccMembersIds = redccRole.members.map((member) => member.id);
  const redccMembersStats = await adminUsersRepo.findByIds(redccMembersIds);

  for (const adminUser of redccMembersStats) {
    const member = redccRole.members.get(adminUser.user_id)!;

    table.addRow(
      member.user.username,
      adminUser.reports,
      adminUser.week_reports,
      adminUser.mutes,
      adminUser.warns,
      adminUser.bans,
    );
  }

  const result = table.toString();
  const buffer = Buffer.from(result);
  const txtFile = new MessageAttachment(buffer, 'rstats.txt');

  await message.author.send({ files: [txtFile] });
};
