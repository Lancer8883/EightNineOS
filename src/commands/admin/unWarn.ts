import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { sendWarnLog } from '../../logs/admin/channels';

export const unwarn = async (message: Message): Promise<void> => {
  const target = message.mentions.users.first();
  if (!target) return;

  const targetUser = await UsersEntity.getOrCreateUser(target.id);

  if (targetUser.warns <= 0) return;

  targetUser.warns--;
  const embed = new MessageEmbed()
    .setColor('BLUE')
    .setTitle('Вам сняли варн')
    .addField('Количество варнов', targetUser.warns + '/3');
  target.send({ embeds: [embed] });

  sendWarnLog(`${message.author} снял варн пользователю ${target} ${targetUser.warns}/3`);
  targetUser.save();
};
