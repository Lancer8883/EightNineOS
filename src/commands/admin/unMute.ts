import { Message, MessageEmbed } from 'discord.js';
import { PenaltiesEntity } from '../../database/entities/Penalties.entity';
import { getRepository } from 'typeorm';
import { getConfig } from '../../config/config';
import { sendMuteLog } from '../../logs/admin/channels';

export const unmute = async (message: Message): Promise<void> => {
  const target = message.mentions.members?.first();
  if (!target) return;

  const repository = getRepository(PenaltiesEntity);
  const [commandName] = message.content.substring(1).split(/\s+/g);
  const isMute = commandName.toLowerCase() === 'unmute';
  const roleID = isMute ? getConfig().roles.mute : getConfig().roles.muteChat;

  const isDBMuted = await PenaltiesEntity.getPenalty(target.id, isMute);

  const isRoleMuted = target.roles.cache.has(roleID);

  if (!isRoleMuted || !isDBMuted)
    throw `Данный пользователь не имеет ${isMute ? 'мута' : 'мута на чат'}`;

  const embed = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(`:speaker: Снятие ${isMute ? 'мута' : 'мута на чат'}`)
    .setDescription(
      `:man_white_haired: Белый господин по имени ${message.author} досрочно снял вам наказание.`,
    );

  if (isDBMuted) await repository.remove(isDBMuted);

  if (isRoleMuted) {
    target.roles.remove(roleID);
    target.send({ embeds: [embed] });
    sendMuteLog(
      `:speaker: ${message.author} снял ${isMute ? 'мут' : 'мут на чат'} пользователю ${target}`,
    );
  }
};
