import { Message, TextChannel } from 'discord.js';
import { CommandsText } from '../../logs/commands/text';
import { getConfig } from '../../config/config';

export const delpic = async (message: Message, user: null, messageId: string): Promise<void> => {
  const { author } = message;

  if (isNaN(parseInt(messageId))) throw CommandsText.PARAMS_ERROR;

  const channel = message.guild!.channels.cache.get(getConfig().channels.text.nsfw) as TextChannel;
  const targetMessage = await channel.messages.fetch(messageId);
  if (targetMessage?.embeds[0].description === `<@${author.id}>`) await targetMessage.delete();
};
