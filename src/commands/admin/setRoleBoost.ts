import { Message } from 'discord.js';
import { RoleBoostEntity } from '../../database/entities/RoleBoost.entity';
import { getRepository } from 'typeorm';

export const setRoleBoost = async (
  message: Message,
  user: null,
  roleID: string,
  toBoostStr: string,
): Promise<void> => {
  const toBoost = parseFloat(toBoostStr);

  if (toBoost >= 5 || toBoost <= -5) return;
  let role = await getRepository(RoleBoostEntity).findOne(roleID);

  if (role) role.to_boost = toBoost;
  else {
    role = new RoleBoostEntity();
    role.role_id = roleID;
    role.to_boost = toBoost;
  }

  await role.save();
  await message.author.send('+');
};
