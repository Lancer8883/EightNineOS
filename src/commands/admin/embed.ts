import { Message, MessageEmbed } from 'discord.js';

export const embed = (message: Message): void => {
  const embed = message.content.slice(7);

  try {
    message.channel.send({ embeds: [new MessageEmbed(JSON.parse(embed))] });
  } catch (e) {
    const embedError = new MessageEmbed()
      .setTitle('Произошла ошибка при обработке эмбеда')
      .setDescription('Проверьте наличие всех скобок, возможно, вы их упустили.')
      .addField(e.name, e.message)
      .setColor('#ff0000')
      .setTimestamp();
    return void message.author.send({ embeds: [embedError] });
  }
};
