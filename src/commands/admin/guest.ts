import { Message } from 'discord.js';
import { getConfig } from '../../config/config';

export const guest = async (message: Message): Promise<void> => {
  const target = message.mentions.users.first();
  if (!target) {
    return;
  }

  const targetMember = await message.guild?.members.fetch(target.id);
  if (!targetMember) {
    return;
  }

  targetMember.roles.add([
    getConfig().roles.guest,
    getConfig().roles.banEvents,
    getConfig().roles.banFilms,
  ]);
};
