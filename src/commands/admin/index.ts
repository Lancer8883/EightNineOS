import { Commands } from '../Commands';
import { warn } from './warn';
import { ban } from './ban';
import { unwarn } from './unWarn';
import { mute } from './mute/mute';
import { unmute } from './unMute';
import { astats } from './astats';
import { rstats } from './rstats';
import { embed } from './embed';
import { setRoleBoost } from './setRoleBoost';
import { getConfig } from '../../config/config';
import { setRole } from './roles/setRole';
import { deleteRole } from './roles/deleteRole';
import { giveRole } from './roles/giveRole';
import { takeRole } from './roles/takeRole';
import { delrep } from './delrep';
import { moveStats } from './moveStats';
import { penalty } from './penalty';
import { delpic } from './delpic';
import { lock } from './lock';
import { unlock } from './unlock';
import { clear } from './clear';
import { mpush } from './messages/mpush';
import { mdel } from './messages/mdel';
import { mlist } from './messages/mlist';
import { guest } from './guest';
import { embededit } from './embededit';

export const adminCommands: Commands = {
  warn: {
    description: 'Выдать варн пользователю',
    params: ['@пользователь', 'причина'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: warn,
  },

  unwarn: {
    description: '',
    params: ['@пользователь'],
    allowedRoles: [getConfig().roles.admin],
    allowedUsers: [...getConfig().unWarnCommandUsers],
    database: false,
    execute: unwarn,
  },

  ban: {
    description: 'Забанить пользователя',
    params: ['@пользователь', 'причина'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: ban,
  },

  mute: {
    description: 'Замутить додика',
    params: ['@пользователь', 'Количество минут', 'причина'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: mute,
  },

  mutechat: {
    description: 'Замутить додика в чате',
    params: ['@пользователь', 'Количество минут', 'причина'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: mute,
  },

  unmute: {
    description: 'Снятие мута',
    params: ['@пользователь'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: unmute,
  },

  unmutechat: {
    description: 'Снятие мута на чат',
    params: ['@пользователь'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: unmute,
  },

  astats: {
    description: '',
    params: [],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: astats,
  },

  rstats: {
    description: '',
    params: [],
    allowedRoles: [
      getConfig().roles.headRedactor,
      getConfig().roles.admin,
      getConfig().roles.moderator,
    ],
    database: false,
    execute: rstats,
  },

  embed: {
    description: '',
    params: [],
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator, getConfig().roles.embed],
    database: false,
    execute: embed,
  },

  embededit: {
    description: '',
    params: [],
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator, getConfig().roles.embed],
    database: false,
    execute: embededit,
  },

  setroleboost: {
    description: '',
    params: ['roleID', 'toBoost'],
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator],
    database: false,
    execute: setRoleBoost,
  },

  setrole: {
    description: '',
    params: ['roleToAccess', 'roleToChange'],
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator],
    database: false,
    execute: setRole,
  },

  delrole: {
    description: '',
    params: ['roleToRemoveAccess', 'roleToChange'],
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator],
    database: false,
    execute: deleteRole,
  },

  giverole: {
    description: '',
    params: ['@пользователь', 'id роли'],
    allowedRoles: [],
    database: false,
    execute: giveRole,
  },

  takerole: {
    description: '',
    params: ['@пользователь', 'id роли'],
    allowedRoles: [],
    database: false,
    execute: takeRole,
  },

  delrep: {
    description: 'Удаление репортов с человека',
    params: [''],
    allowedRoles: [
      getConfig().roles.admin,
      getConfig().roles.moderator,
      getConfig().roles.headRedactor,
    ],
    database: false,
    execute: delrep,
  },

  movestats: {
    description: 'Перемещение статистики',
    params: ['oldID', 'newID'],
    allowedRoles: [
      getConfig().unWarnCommandUsers,
      getConfig().roles.admin,
      getConfig().roles.moderator,
    ],
    database: false,
    execute: moveStats,
  },

  penalty: {
    description: 'Штраф за АФК',
    params: [''],
    allowedRoles: [
      getConfig().roles.admin,
      getConfig().roles.moderator,
      getConfig().roles.headRedactor,
      getConfig().roles.redactor,
    ],
    database: false,
    execute: penalty,
  },

  delpic: {
    description: 'Удаление сообщение с NSFW канала',
    params: ['ID_сообщения'],
    allowedRoles: [getConfig().roles.nsfw],
    database: false,
    execute: delpic,
  },

  lock: {
    description: 'блокирование войс/текст каналов',
    params: [''],
    allowedRoles: [getConfig().roles.moderator],
    database: false,
    execute: lock,
  },

  unlock: {
    description: 'разблокировка войс/текст каналов',
    params: [''],
    allowedRoles: [getConfig().roles.moderator],
    database: false,
    execute: unlock,
  },

  mpush: {
    description: 'Добавить или обновить пасту',
    params: ['Название', 'Код пасты'],
    allowedRoles: [getConfig().roles.moderator],
    database: false,
    execute: mpush,
  },

  mdel: {
    description: 'Удалить пасту',
    params: ['Название'],
    allowedRoles: [getConfig().roles.moderator],
    database: false,
    execute: mdel,
  },

  mlist: {
    description: 'Список паст',
    params: [],
    allowedRoles: [getConfig().roles.moderator],
    database: false,
    execute: mlist,
  },

  clr: {
    description: 'удаление n - ого количества сообщений',
    params: ['count'],
    allowedRoles: [
      getConfig().roles.admin,
      getConfig().roles.moderator,
      getConfig().roles.headRedactor,
    ],
    database: false,
    execute: clear,
  },

  guest: {
    description: 'выдать человеку гостя, временно деффективного и отстранённого',
    params: ['@пользователь'],
    allowedRoles: [...getConfig().roles.admins],
    database: false,
    execute: guest,
  },
};
