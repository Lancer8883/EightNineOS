import { Message } from 'discord.js';
import { getRepository } from 'typeorm';
import { RoleEntity } from '../../../database/entities/Role.entity';
import { sendRoleLog } from '../../../logs/admin/channels';

export const setRole = async (
  message: Message,
  user: null,
  toGiveAccessRoleId: string,
  roleToChangeSetsId: string,
): Promise<void> => {
  const accessRole = message.guild?.roles.cache.get(toGiveAccessRoleId);
  if (!accessRole) return;

  const toChangeRole = message.guild?.roles.cache.get(roleToChangeSetsId);
  if (!toChangeRole) return;

  const roleDoc = await getRepository(RoleEntity).findOne({
    role_id: toChangeRole.id,
  });

  if (roleDoc?.access_roles.includes(accessRole.id)) return;

  if (roleDoc) {
    roleDoc.access_roles.push(accessRole.id);

    await roleDoc.save();
  } else {
    const newDoc = new RoleEntity();

    newDoc.access_roles = [toGiveAccessRoleId];
    newDoc.role_id = toChangeRole.id;
    newDoc.access_members = [];

    await newDoc.save();
  }

  const text = `**Доступ к роли \`${toChangeRole.name}\` получили участники с ролью \`${accessRole.name}\`**`;

  sendRoleLog(
    `${message.author.tag} выдает доступ к роли <@&${toChangeRole.id}> участникам с ролью <@&${accessRole.id}>`,
  );

  message.author.send(text).catch(() => null);
};
