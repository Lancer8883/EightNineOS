import { Message } from 'discord.js';
import { getRepository } from 'typeorm';
import { RoleEntity } from '../../../database/entities/Role.entity';
import { CommandsText } from '../../../logs/commands/text';
import { getMemberById } from '../../../utils/members';
import { sendRoleLog } from '../../../logs/admin/channels';

export const giveRole = async (message: Message, user: null, roleId: string): Promise<void> => {
  const userToAdd = message.mentions.users.first();
  if (!userToAdd) return;

  const authorMember = await message.guild?.members.fetch(message.author.id);
  if (!authorMember) return;

  const memberToAdd = await getMemberById(message.guild!, userToAdd.id);
  if (!memberToAdd) throw CommandsText.NO_MEMBER_FOUND;

  if (!roleId) return;

  const roleDoc = await getRepository(RoleEntity).findOne({
    role_id: roleId,
  });
  if (!roleDoc?.canUse(authorMember)) throw CommandsText.NO_PERMS;

  const role = message.guild?.roles.cache.get(roleId);
  if (!role) return;

  await memberToAdd.roles.add(roleId).catch(() => {});

  sendRoleLog(`${message.author} выдаёт роль <@&${role.id}>`);

  message.author.send('Роль выдана').catch(() => {});
};
