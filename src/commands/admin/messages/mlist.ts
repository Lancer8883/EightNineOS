import { Message, MessageAttachment } from 'discord.js';
import { getRepository } from 'typeorm';
import { MessagesEntity } from '../../../database/entities/Messages.entity';
import { isEmpty } from 'lodash';
import AsciiTable from 'ascii-table';

export const mlist = async (message: Message): Promise<void> => {
  const pastes = await getRepository(MessagesEntity).find();
  if (isEmpty(pastes)) return;

  const table = new AsciiTable('Пасты');
  table.setHeading('Название', 'Код');

  for (const paste of pastes) {
    table.addRow(paste.title, paste.message);
  }

  const result = table.toString();
  const buffer = Buffer.from(result);
  const txtFile = new MessageAttachment(buffer, 'rstats.txt');

  message.author.send({ files: [txtFile] });
};
