import { CategoryChannel, Message, VoiceChannel } from 'discord.js';
import { sendGeneral } from '../../logs/admin/channels';
import { getConfig } from '../../config/config';
import { noun } from 'plural-ru';

export const lock = async (message: Message): Promise<void> => {
  const textCategory = message.guild!.channels.cache.get(getConfig().channels.text.general)!
    .parent as CategoryChannel;
  const moderateVoiceCategory = message.guild!.channels.cache.get(
    getConfig().channels.voice.goodCommunication,
  )!.parent as CategoryChannel;
  const notModerateVoiceCategory = message.guild!.channels.cache.get(
    getConfig().channels.voice.badCommunication,
  )!.parent as CategoryChannel;
  const createPrivate = message.guild!.channels.cache.get(
    getConfig().channels.voice.createRoom,
  )! as VoiceChannel;

  const toLockVoiceCategories = [moderateVoiceCategory, notModerateVoiceCategory];

  const everyone = message.guild!.roles.everyone;

  for (const [, voice] of textCategory!.children) {
    await voice.permissionOverwrites.create(everyone, {
      SEND_MESSAGES: false,
    });
  }

  for (const category of toLockVoiceCategories) {
    for (const [, voice] of category!.children) {
      await voice.permissionOverwrites.create(everyone, {
        CONNECT: false,
      });
    }
  }

  await createPrivate.permissionOverwrites.create(everyone, {
    CONNECT: false,
  });

  const sumTextChannels =
    moderateVoiceCategory!.children.size + notModerateVoiceCategory!.children.size + 1;
  const sumVoiceChannels = textCategory!.children.size;

  sendGeneral(
    `🔒 **Заблокировано**: ${sumTextChannels} текстовых ${noun(
      sumTextChannels,
      'канал',
      'канала',
      'каналов',
    )}, ${sumVoiceChannels} голосовых ${noun(sumVoiceChannels, 'канал', 'канала', 'каналов')}. 🔒`,
  );
};
