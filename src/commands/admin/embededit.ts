import { Message, MessageEmbed } from 'discord.js';

export const embededit = async (
  message: Message,
  user: null,
  messageId: string,
  ...args: string[]
): Promise<void> => {
  const embed = args.join(' ');
  const oldMessage = await message.channel.messages.fetch(messageId);

  try {
    await oldMessage.edit({ embeds: [new MessageEmbed(JSON.parse(embed))] });
  } catch (e) {
    const embedError = new MessageEmbed()
      .setTitle('Произошла ошибка при обработке эмбеда')
      .setDescription('Проверьте наличие всех скобок, возможно, вы их упустили.')
      .addField(e.name, e.message)
      .setColor('#ff0000')
      .setTimestamp();
    return void message.author.send({ embeds: [embedError] });
  }
};
