import { client } from '../client';
import { Command } from './Commands';
import { UsersEntity } from '../database/entities/Users.entity';
import { Message, MessageEmbed } from 'discord.js';
import { getConfig } from '../config/config';
import _ from 'lodash';

const messageHandler = async (message: Message): Promise<void> => {
  if (message.channel.type === 'DM') return;
  if (message.content?.[0] !== getConfig().prefix) return;

  if (message.deletable) {
    setTimeout(() => message.delete().catch(() => {}), 5000);
  }

  const [commandName, ...args] = message.content.substring(1).split(/\s+/g);
  const command = Command[commandName.toLowerCase()];
  if (!command) return;

  const passedTests =
    isPassedAllowedRoles(message, command) &&
    (await isPassedAllowedPermissions(message, command)) &&
    isAllowedToUser(message, command);
  if (!passedTests) return;

  let user: UsersEntity | null;
  if (command.database) {
    user = (await UsersEntity.getOrCreateUser(
      message.author.id,
      command.databaseOptional?.clan,
    )) as UsersEntity;

    if (!user.clan && command.databaseOptional?.clan) return;
  } else {
    user = null;
  }

  try {
    await command.execute(message, user, ...args);
  } catch (e) {
    if (e instanceof MessageEmbed) {
      message.author.send({ embeds: [e] });
    } else {
      message.author
        .send({
          embeds: [
            new MessageEmbed().setColor('RED').setTitle('Ошибка').setDescription(e.toString()),
          ],
        })
        .catch(() => {});
    }
  }
};

const isPassedAllowedRoles = (message: Message, command: Command): boolean => {
  if (_.isEmpty(command.allowedRoles)) return true;
  return command.allowedRoles!.some((role) => message.member?.roles.cache.has(role));
};

const isPassedAllowedPermissions = async (message: Message, command: Command): Promise<boolean> => {
  const authorMember = message.member || (await message.guild!.members.fetch(message.author.id));
  if (_.isEmpty(command.allowedPermissions)) return true;
  return command.allowedPermissions!.some((per) => authorMember.permissions.has(per));
};

const isAllowedToUser = (message: Message, command: Command): boolean => {
  if (_.isEmpty(command.allowedUsers)) return true;
  return command.allowedUsers!.some((userID) => userID === message.author.id);
};

client.on('messageCreate', messageHandler);

client.on('messageUpdate', (_: Message, newMessage: Message) => messageHandler(newMessage));
