import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { CommandsText } from '../../logs/commands/text';
import { noun } from 'plural-ru';
import { sendClanLog } from '../../logs/admin/channels';
import { getConfig } from '../../config/config';

export const addClanCoins = async (
  message: Message,
  user: UsersEntity,
  strSum: string,
): Promise<void> => {
  const sum = parseInt(strSum);
  if (isNaN(sum) || sum <= 0) throw CommandsText.PARAMS_ERROR;

  if (sum < 300) throw CommandsText.TRANSFER_MIN_CLAN;

  const commission = getConfig().commission * sum;
  const neededCoins = commission + sum;

  if (user.coins < neededCoins) {
    const enoughCoins = neededCoins - user.coins;
    const sumRus = noun(sum, 'коин', 'коина', 'коинов');
    const commissionRus = noun(commission, 'коин', 'коина', 'коинов');
    const enoughCoinsRus = noun(enoughCoins, 'коин', 'коина', 'коинов');
    const userCoins = noun(user.coins, 'коин', 'коина', 'коинов');

    throw new MessageEmbed()
      .setColor('RED')
      .setTitle(`Недостаточно коинов`)
      .addField('Не хватает', enoughCoins.toFixed(1) + ' ' + enoughCoinsRus, true)
      .addField('Сумма перевода', sum + ' ' + sumRus, true)
      .addField('Комиссия', commission.toFixed(1) + ' ' + commissionRus, true)
      .setFooter('Баланс: ' + user.coins + ' ' + userCoins);
  }

  await user.takeCoins(neededCoins);
  user.clan.coins += sum;

  await user.clan.save();

  const sendText = `${message.author} перевел **${sum}** ${noun(
    sum,
    'коин',
    'коина',
    'коинов',
  )} на счет Вашего клана.`;

  const sendTextLog = `${message.author} перевел **${sum}** ${noun(
    sum,
    'коин',
    'коина',
    'коинов',
  )} на счет клана **${user.clan.name}**.`;

  sendClanLog(sendTextLog);
  user.clan.sendMessageToOwnerAndDeputy(sendText);
};
