import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import { CommandsText } from '../../logs/commands/text';
import { ClansEntity } from '../../database/entities/Clans.entity';
import { getRepository } from 'typeorm';
import { getConfig } from '../../config/config';
import moment from 'moment';
import { sendClanLog, sendGeneral } from '../../logs/admin/channels';

export const buyClan = async (message: Message, user: UsersEntity): Promise<void> => {
  const repository = getRepository(ClansEntity);
  const isClanLeaderAndParticipant = await repository.findOne({
    where: [{ leader: message.author.id }, { deputy: message.author.id }],
  });

  if (isClanLeaderAndParticipant || user.clan) return;
  if (user.coins < getConfig().clanPrice) throw CommandsText.NO_COINS;

  const dateNow = moment();
  const name = `Клан ${message.author.username}`;

  const role = await message.guild!.roles.create({
    name,
  });

  const authorMember = message.member || (await message.guild!.members.fetch(message.author.id));
  if (!authorMember) return;
  await authorMember.roles.add(role);

  const clanInstance = repository.create({
    // @ts-ignore
    role: role.id,
    date: dateNow.toDate(),
    deputy: null,
    leader: message.author.id,
    emblem: '',
    room: null,
    name,
  });

  user.clan = await clanInstance.save();
  user.clan.clanChannelChat(message.guild!, true);

  await sendClanLog(`${message.author} создал клан "**${user.clan.name}**"`);
  await sendGeneral(`${message.author} создал клан "**${user.clan.name}**"`);

  const embed = new MessageEmbed().setColor('BLUE').setTitle(CommandsText.CLAN_CREATED);
  await message.author.send({ embeds: [embed] });
  await user.takeCoins(getConfig().clanPrice);
};
