import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../database/entities/Users.entity';
import moment from 'moment';
import { getConfig } from '../../config/config';

export const clanInfo = async (message: Message, user: UsersEntity): Promise<void> => {
  const currentTime = moment();
  const attackText =
    currentTime.isAfter(user.clan.attack_timeout) || user.clan.attack_timeout === null
      ? 'Вы можете атаковать сейчас!'
      : moment(user.clan.attack_timeout).fromNow();
  const countPower = await user.clan.countPower();
  const countMembers = await user.clan.countMembers();
  const role = await message.guild!.roles.fetch(user.clan.role);

  const clanPrice = getConfig().clanExtendPrice + countMembers * 300;
  const embed = new MessageEmbed()
    .setColor(role!.hexColor)
    .setTitle(`Статистика Клана **${user.clan.name}**:`)
    .addField('Уровень', user.clan.level.toString(), true)
    .addField('Коинов', Math.floor(user.clan.coins).toString(), true)
    .addField('Стоимость оплаты', Math.floor(clanPrice).toString(), true)
    .addField('Участников', countMembers.toString(), true)
    .addField('Сила', countPower.toString(), true)
    .addField('Следующая атака доступна', attackText, true)
    .setImage(user.clan.emblem);
  await message.author.send({ embeds: [embed] });
};
