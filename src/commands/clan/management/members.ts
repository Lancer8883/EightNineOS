import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { noun } from 'plural-ru';

export const members = async (message: Message, user: UsersEntity): Promise<void> => {
  if (!user.clan.isClanLeaderOrDeputy(user)) return;

  const members = await user.clan.getMembers();
  const text = members
    .map((user) => {
      return `<@!${user.user_id}> - ${user.getEquipment()}  ${noun(
        user.getEquipment(),
        'сила',
        'силы',
      )}`;
    })
    .join('\n');
  const role = await message.guild!.roles.fetch(user.clan.role);

  message.author.send({
    embeds: [{ color: role?.hexColor, title: user.clan.name, description: text }],
  });
};
