import { HexColorString, Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';

const HEX_REGULAR = /^#([0-9A-F]{3}){1,2}$/i;
const isHexColor = (hex: string) => HEX_REGULAR.test(hex);

export const setClanColor = async (
  message: Message,
  user: UsersEntity,
  hex: HexColorString,
): Promise<void> => {
  if (!isHexColor(hex)) throw CommandsText.NOT_HEX_COLOR;

  const role = await message.guild!.roles.fetch(user.clan.role);
  role?.setColor(hex);
};
