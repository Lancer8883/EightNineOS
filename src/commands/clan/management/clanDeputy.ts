import { Message, MessageEmbed } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';

export const clanDeputy = async (message: Message, user: UsersEntity): Promise<void> => {
  const newDeputy = message.mentions.members!.first();
  if (message.author.id !== user.clan.leader) return;

  if (!newDeputy) {
    if (!user.clan.deputy) throw CommandsText.NO_DEPUTY;

    const embed = new MessageEmbed()
      .setColor('BLUE')
      .setTitle(CommandsText.SUCCESSFULLY_KICKED_DEPUTY);

    // @ts-ignore
    user.clan.deputy = null;
    message.author.send({ embeds: [embed] });
    return;
  }

  const targetDeputyUser = await UsersEntity.getOrCreateUser(newDeputy.id);

  if (targetDeputyUser.clan !== user.clan.id) throw CommandsText.NOT_IN_YOURS_CLAN;

  if (newDeputy.id === user.clan.deputy) throw CommandsText.ALREADY_DEPUTY;

  if (message.author.id === newDeputy.id) throw CommandsText.CANT_SELF_SET_AS_DEPUTY;

  user.clan.deputy = newDeputy.id;
  const embedSetDeputy = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(CommandsText.YOU_SUCCESSFULLY_SET_DEPUTY);

  const embedToDeputy = new MessageEmbed()
    .setColor('BLUE')
    .setTitle(`Вас назначили заместителем клана **${user.clan.name}**`);
  message.author.send({ embeds: [embedSetDeputy] });
  newDeputy.send({ embeds: [embedToDeputy] });

  user.clan.save();
};
