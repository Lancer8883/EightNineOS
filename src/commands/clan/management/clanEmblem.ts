import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';

const imageRegular = /(jpg|jpeg|gif|png)((\?.*)$|$)/gm;

export const clanEmblem = (message: Message, user: UsersEntity, ...urlArr: string[]): void => {
  if (!user.clan.isClanLeaderOrDeputy(user)) return;

  const url = urlArr.join(' ');
  if (!imageRegular.test(url)) return;

  user.clan.emblem = url;
  user.clan.save();
};
