import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';
import { sendClanLog } from '../../../logs/admin/channels';
import { noun } from 'plural-ru';

export const takeClansCoins = async (
  message: Message,
  user: UsersEntity,
  strSum: string,
): Promise<void> => {
  const sum = parseInt(strSum);

  if (isNaN(sum) || sum <= 0) throw CommandsText.PARAMS_ERROR;

  if (!user.clan.isClanLeaderOrDeputy(user)) throw CommandsText.USAGE_ONLY_FOR_LEADER;

  if (sum > user.clan.coins) throw CommandsText.NO_MONEY_IN_CLAN_BANK;

  user.clan.coins -= sum;

  await user.clan.save();
  await user.giveCoins(sum);

  sendClanLog(
    `${message.author} снял **${sum}** ${noun(sum, 'коин', 'коина', 'коинов')} с счета клана **${
      user.clan.name
    }**.`,
  );
};
