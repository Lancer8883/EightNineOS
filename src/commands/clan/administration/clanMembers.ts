import { Message, MessageEmbed } from 'discord.js';
import { noun } from 'plural-ru';
import { ClansEntity } from '../../../database/entities/Clans.entity';
import { getRepository, PessimisticLockTransactionRequiredError } from 'typeorm';
import { CommandsText } from '../../../logs/commands/text';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const clanMembers = async (message: Message, user: UsersEntity, chat_id: string): Promise<void> => {
  const clanRepository = getRepository(ClansEntity);
  const clan = await clanRepository
    .createQueryBuilder('clan')
    .where('clan.chatRoom = :room_id', {
      room_id: chat_id,
    })
    .getOne();

  if (!clan) throw CommandsText.NO_CLAN;

  const members = await clan.getMembers();
  const leader = await clan.leader;
  const text = members
    .map((user) => {
      return `<@!${user.user_id}>`;
    })
    .join('\n');

  const embed = new MessageEmbed()
    .setTitle(clan.name)
    .setDescription(text)
    .addField('Глава', '<@' + leader + '>', false)
  message.author.send({
    embeds: [embed],
  });
};
