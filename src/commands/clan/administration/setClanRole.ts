import { Message, MessageEmbed } from 'discord.js';
import { ClansEntity } from '../../../database/entities/Clans.entity';
import { getRepository } from 'typeorm';
import { CommandsText } from '../../../logs/commands/text';
import { UsersEntity } from '../../../database/entities/Users.entity';

export const setClanRole = async (message: Message, user: UsersEntity, chat_id: string, role_id: string): Promise<void> => {
    const clanRepository = getRepository(ClansEntity);
    const clan = await clanRepository
        .createQueryBuilder('clan')
        .where('clan.chatRoom = :room_id', {
            room_id: chat_id,
        })
        .getOne();

    if (!clan) throw CommandsText.NO_CLAN;

    const old_role = clan.role;

    clan.role = role_id;
    await clan.save();
    const embed = new MessageEmbed()
        .setTitle('Роль клана сменена')
        .addField('Старый id роли', old_role, true)
        .addField('Новый id роли', clan.role, true)
    message.author.send({
        embeds: [embed],
    }).catch(() => { });
};