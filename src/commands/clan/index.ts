import { Commands } from '../Commands';
import { buyClan } from './buyClan';
import { clanname } from './management/clanName';
import { clanEmblem } from './management/clanEmblem';
import { invite } from './invite';
import { leaveClan } from './leave';
import { clanDelete } from './management/clanDelete';
import { kickClan } from './management/kick';
import { clanInfo } from './clan';
import { addClanCoins } from './addClanCoins';
import { takeClansCoins } from './management/takeClanCoins';
import { topClans } from './topClans';
import { setClanColor } from './management/clanColor';
import { clanDeputy } from './management/clanDeputy';
import { members } from './management/members';
import { terInfo } from './territory/terInfo';
import { dropTer } from './territory/dropTer';
import { terList } from './territory/terList';
import { terWar } from './territory/terWar';
import { clanChat } from './management/сlanChat';
import { clanIcon } from './management/clanIcon';
import { clanMembers } from './administration/clanMembers';
import { setClanRole } from './administration/setClanRole';
import { getConfig } from '../../config/config';

export const clanCommands: Commands = {
  buyclan: {
    params: [],
    description: 'Приобрести клан',
    database: true,
    execute: buyClan,
  },

  clanicon: {
    params: ['URL иконки'],
    description: 'Установить иконку роли клана',
    database: true,
    execute: clanIcon,
    databaseOptional: {
      clan: true,
    },
  },

  clanname: {
    params: ['Название клана'],
    description: 'Установить название клана',
    database: true,
    execute: clanname,
    databaseOptional: {
      clan: true,
    },
  },

  clanemblem: {
    params: ['URL эмблемы'],
    description: 'Установить эмблему клана',
    database: true,
    execute: clanEmblem,
    databaseOptional: {
      clan: true,
    },
  },

  invite: {
    params: ['@пользователь'],
    description: 'Пригласить пользователя в клан',
    database: true,
    execute: invite,
    databaseOptional: {
      clan: true,
    },
  },

  leave: {
    params: [],
    description: 'Покинуть клан',
    database: true,
    execute: leaveClan,
    databaseOptional: {
      clan: true,
    },
  },

  kick: {
    params: ['@пользователь'],
    description: 'Кикнуть из клана',
    database: true,
    execute: kickClan,
    databaseOptional: {
      clan: true,
    },
  },

  clanchat: {
    params: ['@пользователь'],
    description: 'Добавить/удалить из чата пользователя',
    database: true,
    execute: clanChat,
    databaseOptional: {
      clan: true,
    },
  },

  clandelete: {
    params: [],
    description: 'Удалить клан',
    database: true,
    execute: clanDelete,
    databaseOptional: {
      clan: true,
    },
  },

  clan: {
    params: [],
    description: 'Посмотреть информацию о клане',
    database: true,
    execute: clanInfo,
    databaseOptional: {
      clan: true,
    },
  },

  gcc: {
    params: ['количество коинов'],
    description: 'Перевести коины на счет своего клана',
    database: true,
    execute: addClanCoins,
    databaseOptional: {
      clan: true,
    },
  },

  tcc: {
    params: ['количество коинов'],
    description: 'Перевести коины со счета клана на свой',
    database: true,
    execute: takeClansCoins,
    databaseOptional: {
      clan: true,
    },
  },

  topclans: {
    params: [],
    description: 'Вывести топ кланов по коинам',
    database: false,
    execute: topClans,
  },

  clancolor: {
    params: ['HEX-цвет'],
    description: 'Поменять клану цвет',
    database: true,
    execute: setClanColor,
    databaseOptional: {
      clan: true,
    },
  },

  clanzam: {
    params: ['member'],
    description: 'Назначить нового заместителя клана',
    database: true,
    execute: clanDeputy,
    databaseOptional: {
      clan: true,
    },
  },

  members: {
    params: [],
    description: 'Информация о участниках клана',
    database: true,
    execute: members,
    databaseOptional: {
      clan: true,
    },
  },

  terinfo: {
    params: ['Номер территории'],
    description: 'Информация о контроле территорий кланами',
    database: false,
    execute: terInfo,
  },

  dropter: {
    params: ['Номер территории'],
    description: 'Сбросить территорию',
    database: true,
    execute: dropTer,
    databaseOptional: {
      clan: true,
    },
  },

  terlist: {
    params: [],
    description: 'Посмотреть список территории',
    database: false,
    execute: terList,
  },

  terwar: {
    params: ['Номер территории'],
    description: 'Объявить войну клану за территорию',
    database: true,
    execute: terWar,
    databaseOptional: {
      clan: true,
    },
  },
  
  clanmembers: {
    params: ['id текстового канала клана'],
    description: 'Просмотр всех участников клана',
    database: true,
    execute: clanMembers,
    databaseOptional: {
      clan: true,
    },
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator],
  },

  setclanrole: {
    params: ['id текстового канала клана'],
    description: 'Смена роли клана',
    database: true,
    execute: setClanRole,
    databaseOptional: {
      clan: true,
    },
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator],
  },

};
