import { Message } from 'discord.js';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { CommandsText } from '../../../logs/commands/text';
import { TerritoryEntity } from '../../../database/entities/Territory.entity';
import { getRepository } from 'typeorm';
import { ClansEntity } from '../../../database/entities/Clans.entity';
import { sendClanLog, sendGeneral } from '../../../logs/admin/channels';
import moment from 'moment';

export const terWar = async (
  message: Message,
  user: UsersEntity,
  attackTerritoryStr: string,
): Promise<void> => {
  const attackTerritoryID = parseInt(attackTerritoryStr);
  if (isNaN(attackTerritoryID)) throw CommandsText.PARAMS_ERROR;

  if (!user.clan.isClanLeaderOrDeputy(user)) return;

  const attackTerritory = await TerritoryEntity.getTerritory(attackTerritoryID);
  if (!attackTerritory) return;

  const repository = getRepository(TerritoryEntity);

  const countTerritoryForClan = await repository.count({
    where: { owner: user.clan.id },
  });

  if (countTerritoryForClan >= 3) throw 'У Вас максимальное количество территорий';
  if (attackTerritory.owner === user.clan.id) throw 'Вы не можете атаковать вашу же территорию.';

  if (moment().isBefore(user.clan.attack_timeout))
    throw `Следующая атака будет доступна ${moment(user.clan.attack_timeout).fromNow()}`;

  if (attackTerritory.freeze)
    throw `Территория **${attackTerritory.name}** заморожена, Вы сможете напасть на неё только после 00:00`;

  if (attackTerritory.price > user.clan.coins) throw 'У вашего клана недостаточно коинов.';

  if (!attackTerritory.owner) {
    message.author.send(`Вы успешно купили территорию **${attackTerritory.name}**`);
    attackTerritory.owner = user.clan.id;

    await user.clan.takeCoins(attackTerritory.price);
    await attackTerritory.save();

    sendGeneral(`**${user.clan.name}** купили территорию **#${attackTerritoryID}**`);
    sendClanLog(`**${user.clan.name}** купили территорию **#${attackTerritoryID}**`);
    return;
  }

  const enemy = (await getRepository(ClansEntity).findOne({
    where: { id: attackTerritory.owner },
  }))!;

  const enemyPower = await enemy.countPower();
  const localPower = await user.clan.countPower();

  const result = Math.random() * (enemyPower + localPower);

  user.clan.attack_timeout = moment().add(4, 'hours').toDate();

  if (result <= localPower) {
    // clan win

    attackTerritory.owner = user.clan.id;
    attackTerritory.freeze = true;

    await user.clan.takeCoins(attackTerritory.price);
    await attackTerritory.save();

    message.author.send(
      `Поздравляем! Вы выиграли бой за территорию **${attackTerritory.name}** у клана **${enemy.name}**!`,
    );
    sendGeneral(
      `**${user.clan.name}** выиграли бой на территорию **#${attackTerritoryID}** у **${enemy.name}**`,
    );
    sendClanLog(
      `**${user.clan.name}** выиграли бой на территорию **#${attackTerritoryID}** у **${enemy.name}**`,
    );

    message.guild!.members.fetch(enemy!.leader).then((leader) => {
      leader?.send(
        `Вы проиграли бой за территорию **${attackTerritory.name}** у клана **${user.clan.name}**`,
      );
    });
  } else {
    await user.clan.takeCoins(attackTerritory.price / 2);

    message.author.send(
      `Вы проиграли бой за территорию **${attackTerritory.name}** у клана **${enemy.name}**`,
    );
    sendGeneral(
      `**${user.clan.name}** проиграли бой на территорию **#${attackTerritoryID}** у **${enemy.name}**`,
    );
    sendClanLog(
      `**${user.clan.name}** проиграли бой на территорию **#${attackTerritoryID}** у **${enemy.name}**`,
    );

    message.guild!.members.fetch(enemy!.leader).then((leader) => {
      leader?.send(
        `Поздравляем! Вы выиграли бой за территорию **${attackTerritory.name}** у клана **${user.clan.name}**!`,
      );
    });
  }
};
