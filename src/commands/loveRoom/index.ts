import { Commands } from '../Commands';
import { cancelMarry } from './invite/cancel';
import { acceptMarry } from './invite/accept';
import { family } from './family';
import { ycustody } from './invite/ycustody';
import { custody } from './invite/custody';
import { addCoins } from './addCoins';
import { divorce } from './divorce';
import { ncustody } from './invite/ncustody';
import { marry } from './invite/marry';
import { takeOrphanage } from './invite/takeOrphanage';
import { setCustody } from './invite/setCustody';
import { getConfig } from '../../config/config';

export const marryCommands: Commands = {
  marry: {
    params: ['@пользователь'],
    description: 'Предложить руку и сердце',
    database: true,
    execute: marry,
  },

  no: {
    params: [''],
    description: 'Отклонить предложение руки и сердца',
    database: false,
    execute: cancelMarry,
  },

  yes: {
    params: [''],
    description: 'Принять предложение руки и сердца',
    database: false,
    execute: acceptMarry,
  },

  family: {
    params: [''],
    description: 'Информация о семье',
    database: false,
    execute: family,
  },

  cus: {
    params: ['@пользователь'],
    description: 'Предложить опекунство',
    database: false,
    execute: custody,
  },

  ycus: {
    params: [''],
    description: 'Принять опекунство',
    database: false,
    execute: ycustody,
  },

  ncus: {
    params: [''],
    description: 'Отклонить опекунство',
    database: false,
    execute: ncustody,
  },

  gcf: {
    params: ['coins'],
    description: 'Пополнить счет семьи',
    database: true,
    execute: addCoins,
  },

  divorce: {
    params: [''],
    description: 'Развод',
    database: true,
    execute: divorce,
  },

  otake: {
    params: [''],
    description: 'Развод',
    database: true,
    execute: takeOrphanage,
  },

  setcus: {
    params: ['@пользователь'],
    allowedRoles: [getConfig().roles.admin, getConfig().roles.moderator],
    description: 'Восстановить опекунство',
    database: false,
    execute: setCustody,
  },
};
