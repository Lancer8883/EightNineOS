import { Message, MessageEmbed } from 'discord.js';
import { getRepository } from 'typeorm';
import { custodyInvites } from './custody';
import { LoveRoomsEntity } from '../../../database/entities/LoveRooms.entity';
import { LoveRoomsChildrenEntity } from '../../../database/entities/LoveRoomsChildren.entity';
import { CommandsText } from '../../../logs/commands/text';
import moment from 'moment';

export const ycustody = async (message: Message): Promise<void> => {
  const targetId = custodyInvites.get(message.author.id);
  if (!targetId) throw CommandsText.NO_REQUESTS;

  const target = await message.guild!.members.fetch(targetId)!;

  const roomRepository = getRepository(LoveRoomsEntity);
  const childrenRepository = getRepository(LoveRoomsChildrenEntity);

  const room = await roomRepository.findOne({
    where: [{ firstUser_id: target.id }, { secondUser_id: target.id }],
    relations: ['children'],
  });

  if (!room) return void custodyInvites.delete(message.author.id);

  if (room.children.length >= 3) throw CommandsText.NOT_RELEVANT;

  target.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Пользователь ${message.author} **стал** вашим ребёнком. Пусть это чудо навсегда **принесет счастье** в ваш дом!`,
        ),
    ],
  });

  message.author.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Вы **стали ребёнком** семьи пользователя ${target}. Приносите **счастье** своим родителям!`,
        ),
    ],
  });
  const channel = await message.guild?.channels.fetch(room.room_id);
  if (!channel) return;

  room.addChildren(message.author.id);

  room.childCD = moment().toDate();
  await room.save();
  const childMember = await message.guild?.members.fetch(message.author.id);
  if (!childMember) return;
  channel.permissionOverwrites.create(childMember.id, { CONNECT: true, VIEW_CHANNEL: true });
  custodyInvites.delete(message.author.id);
};
