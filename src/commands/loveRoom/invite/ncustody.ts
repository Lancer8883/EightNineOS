import { custodyInvites } from './custody';
import { Message, MessageEmbed } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';

export const ncustody = async (message: Message): Promise<void> => {
  const invite = custodyInvites.get(message.author.id);

  if (invite) {
    const custodyAuthor = await message.guild?.members.fetch(
      custodyInvites.get(message.author.id)!,
    );
    if (!custodyAuthor) throw CommandsText.NO_REQUESTS;

    message.author.send({
      embeds: [
        new MessageEmbed()
          .setColor(16426470)
          .setDescription(
            `Вы ответили **отказом** на предложение пользователя ${custodyAuthor} стать вашим ребёнком.\nКажется, вы **разбили** сердце молодой паре... 💔`,
          ),
      ],
    });
    custodyAuthor.send({
      embeds: [
        new MessageEmbed()
          .setColor(16426470)
          .setDescription(
            `Пользователь ${message.author} **отказался** стать вашим ребёнком. Не расстраивайтесь: **лучик счастья** появится и в вашем доме!`,
          ),
      ],
    });
    custodyInvites.delete(message.author.id);
  }
};
