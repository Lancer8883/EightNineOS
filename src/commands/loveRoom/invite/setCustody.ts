import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../../database/entities/LoveRooms.entity';
import { Message, MessageEmbed } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';
import moment from 'moment';
import { getConfig } from '../../../config/config';

export const setCustody = async (message: Message, user: null, room_id: string): Promise<void> => {
  const userToAdd = message.mentions.users.first();
  if (!userToAdd) throw CommandsText.PARAMS_ERROR;
  const roomRepository = getRepository(LoveRoomsEntity);

    const targetFamily = await roomRepository
    .createQueryBuilder('family')
    .leftJoinAndSelect('family.children', 'children')
    .where('family.room_id = :id', {
      id: room_id,
    })
    .getOne();

    const cusFamily = await roomRepository
    .createQueryBuilder('family')
    .leftJoinAndSelect('family.children', 'children')
    .where('children.user_id = :id OR family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: userToAdd.id,
    })
    .getOne();

  if (cusFamily) throw CommandsText.ALREADY_HAS_FAMILY;

  if (!targetFamily) throw CommandsText.NO_FAMILY;

  if (targetFamily.firstUser_id == userToAdd.id || targetFamily.secondUser_id == userToAdd.id)
    throw CommandsText.CANNOT_CUSTODY_SPOUSE;

  if (targetFamily.children.length >= 3) throw CommandsText.MAX_CHILDREN;

  targetFamily.addChildren(userToAdd.id);

  message.author.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Вы добаввили пользователя ${userToAdd} в семью <#${targetFamily.room_id}>`,
        ),
    ],
  });
};
