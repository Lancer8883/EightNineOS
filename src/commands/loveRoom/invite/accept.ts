import { Message, MessageEmbed, TextChannel } from 'discord.js';
import { marryInvites } from './marry';
import { getRepository } from 'typeorm';
import moment from 'moment';
import { LoveRoomsEntity } from '../../../database/entities/LoveRooms.entity';
import { getConfig } from '../../../config/config';
import { UsersEntity } from '../../../database/entities/Users.entity';
import { client } from '../../../client';
import { LoveRoomsChildrenEntity } from '../../../database/entities/LoveRoomsChildren.entity';
import { CommandsText } from '../../../logs/commands/text';

moment.locale('ru');

export const acceptMarry = async (message: Message): Promise<void> => {
  const channel = client.channels.cache.get(getConfig().channels.text.general) as TextChannel;
  const targetId = marryInvites.get(message.author.id);

  if (!targetId) throw CommandsText.NO_REQUESTS;

  const targetMember = await message.guild!.members.fetch(targetId)!;
  const authorMember = await message.guild!.members.fetch(message.author.id);
  if (!authorMember) return;

  const loveRoomRepository = getRepository(LoveRoomsEntity);
  const childRepository = getRepository(LoveRoomsChildrenEntity);
  const user = await UsersEntity.getOrCreateUser(targetId);
  const child = await childRepository.find({ where: [{ user_id: message.author.id }] });

  if (child.length != 0) {
    message.author.send({
      embeds: [new MessageEmbed().setColor(16426470).setDescription(CommandsText.LEFT_FAMILY)],
    });
    for (const children of child) {
      await childRepository.delete(children);
    }
  }

  const room = await message.guild!.channels.create(
    `${message.author.username} 🧡 ${targetMember.user.username}`,
    {
      type: 'GUILD_VOICE',
      userLimit: 2,
      parent: getConfig().channels.voice.loveRoomCategory,
      permissionOverwrites: [
        {
          id: message.guild!.roles.everyone,
          deny: ['CONNECT'],
        },
        {
          id: message.author.id,
          allow: ['CONNECT', 'VIEW_CHANNEL'],
        },
        {
          id: targetMember.id,
          allow: ['CONNECT', 'VIEW_CHANNEL'],
        },
        {
          id: getConfig().roles.moderator,
          allow: ['MOVE_MEMBERS'],
          deny: ['CONNECT'],
        },
      ],
    },
  );

  authorMember.roles.remove(getConfig().roles.orphanage);
  targetMember.roles.remove(getConfig().roles.orphanage);
  authorMember.roles.add(getConfig().roles.loverole);
  targetMember.roles.add(getConfig().roles.loverole);

  const family = loveRoomRepository.create({
    room_id: room.id,
    firstUser_id: message.member!.id,
    secondUser_id: targetMember.id,
    childCD: moment().toDate(),
    balance: 0,
  });

  await family.save();

  user.takeCoins(getConfig().loveRoomExtendPrice);

  marryInvites.delete(message.author.id);

  message.author.send({
    embeds: [
      new MessageEmbed().setColor(16426470)
        .setDescription(`Вы создали семейный очаг, живите долго и счастливо! \nНе забывайте, что каждый месяц на вашу семью приходят налоги, которые списываются с вашего счета, так что не забывайте пополнять его!
  \n Ваша комната: **<#${room.id}>**`),
    ],
  });

  targetMember.send({
    embeds: [
      new MessageEmbed().setColor(16426470)
        .setDescription(`Ваш любовный напарник согласен на создание любовных уз! Живите долго и счастливо!\n
      Не забывайте, что каждый месяц на вашу семью приходят налоги, которые списываются с вашего счета, не забывайте пополнять его!
      \n Ваша комната: **<#${room.id}>**`),
    ],
  });

  channel.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setTitle(`**💞 БРАКОСОЧЕТАНИЕ 💞**`)
        .setDescription(
          `**СВАДЬБА СВАДЬБА СВАДЬБА!!!** 
          \n${message.author} и ${targetMember} создали семью!
          \nВсе вместе желаем им счастья, здоровья, денег и всего самого наилучшего!
          \nВы можете поддержать их своим подарочком, ведь налоги у нас не малые!`,
        )
        .setImage(
          'https://cdn.discordapp.com/attachments/655461766049824778/886654331087110164/giphy.gif',
        ),
    ],
  });
};
