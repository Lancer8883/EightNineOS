import { Message, MessageEmbed } from 'discord.js';
import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../../database/entities/LoveRooms.entity';
import { LoveRoomsChildrenEntity } from '../../../database/entities/LoveRoomsChildren.entity';
import { CommandsText } from '../../../logs/commands/text';
import { getConfig } from '../../../config/config';
import moment from 'moment';

export const takeOrphanage = async (message: Message): Promise<void> => {
  const target = message.mentions.members!.first();
  if (!target) throw CommandsText.PARAMS_ERROR;

  if (target.id == message.author.id) return;

  if (!target.roles.cache.has(getConfig().roles.orphanage)) throw CommandsText.IS_NOT_ORPHAN;

  const roomRepository = getRepository(LoveRoomsEntity);
  const childrenRepository = getRepository(LoveRoomsChildrenEntity);

  const family = await roomRepository.findOne({
    where: [{ firstUser_id: message.author.id }, { secondUser_id: message.author.id }],
    relations: ['children'],
  });

  if (!family) throw CommandsText.NO_FAMILY;

  if (family.children.length >= 3) throw CommandsText.MAX_CHILDREN;

  if (moment(family.childCD) > moment().subtract(3, 'months'))
    throw CommandsText.CANNOT_ACCEPT_CHILD;

  const child = await childrenRepository.create({
    user_id: target.id,
    room_id: family,
  });

  message.author.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Ваша семья зашла в детдом и приметила ребёнка ${target}. Будьте ответственны за тех, кого вы приютили!`,
        ),
    ],
  });

  target.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setTitle(`**О-паньки!?!?!**`)
        .setDescription(
          `В детдом пришла семья пользователя ${message.author}.  Они долго выбирали ребёнка, и их взор упал на вас. Будьте счастливы в новом месте и приносите счастье вашим **новым** родителям!`,
        ),
    ],
  });

  family.childCD = moment().toDate();
  await family.save();
  await childrenRepository.save(child);
  await target.roles.remove(getConfig().roles.orphanage);
};
