import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../../database/entities/LoveRooms.entity';
import { Message, MessageEmbed } from 'discord.js';
import { CommandsText } from '../../../logs/commands/text';
import moment from 'moment';
import { getConfig } from '../../../config/config';

/**
 string - first user id
 string - second user id
 */
export const custodyInvites = new Map<string, string>();

export const custody = async (message: Message): Promise<void> => {
  const roomRepository = getRepository(LoveRoomsEntity);

  const target = message.mentions.members!.first();
  if (!target) throw CommandsText.PARAMS_ERROR;

  if (target.id == message.author.id) throw CommandsText.CANNOT_CUSTODY_YOURSELF;

  const isAlreadyInvited = custodyInvites.has(target.id);
  if (isAlreadyInvited) throw CommandsText.ALREADY_REQUESTED_CUSTODY;

  if (target.roles.cache.has(getConfig().roles.orphanage)) throw CommandsText.IS_ORPHAN;

    const parentFamily = await roomRepository
    .createQueryBuilder('family')
    .leftJoinAndSelect('family.children', 'children')
    .where('children.user_id = :id OR family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: message.author.id,
    })
    .getOne();

    const targetFamily = await roomRepository
    .createQueryBuilder('family')
    .leftJoinAndSelect('family.children', 'children')
    .where('children.user_id = :id OR family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: target.id,
    })
    .getOne();

  if (!parentFamily) throw CommandsText.NO_FAMILY;

  if (targetFamily) throw CommandsText.ALREADY_HAS_FAMILY;

  const family = parentFamily;

  if (family.firstUser_id == target.id || family.secondUser_id == target.id)
    throw CommandsText.CANNOT_CUSTODY_SPOUSE;

  if (moment(family.childCD) > moment().subtract(3, 'months'))
    throw CommandsText.CANNOT_ACCEPT_CHILD;

  if (family.children.length >= 3) throw CommandsText.MAX_CHILDREN;

  // @ts-ignore
  if (family.children.includes({ room_id: family.room_id, user_id: target.id })) return;

  custodyInvites.set(target.id, message.author.id);

  message.author.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Вы отправили пользователю ${target} предложение стать вашим ребёнком. Готов ли он сделать смелый шаг и вступить в вашу семью?`,
        ),
    ],
  });

  target.send({
    embeds: [
      new MessageEmbed()
        .setColor(16426470)
        .setDescription(
          `Пользователь ${message.author} пригласил вас в семью. Готовы ли  вы сделать смелый шаг и вступить в новую семью?`,
        )
        .setFooter(CommandsText.CUSTODY_COMMANDS),
    ],
  });
};
