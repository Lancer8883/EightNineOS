import { Message, MessageEmbed } from 'discord.js';
import { CommandsText } from '../../logs/commands/text';
import { UsersEntity } from '../../database/entities/Users.entity';
import { noun } from 'plural-ru';
import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../database/entities/LoveRooms.entity';
import { sendLoveLog } from '../../logs/admin/channels';

export const addCoins = async (
  message: Message,
  user: UsersEntity,
  strSum: string,
): Promise<void> => {
  const loveRoomRepository = getRepository(LoveRoomsEntity);
  const notFullFamily = await loveRoomRepository
    .createQueryBuilder('family')
    .leftJoinAndSelect('family.children', 'children')
    .where('children.user_id = :id OR family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: message.author.id,
    })
    .getOne();
  if (!notFullFamily) throw CommandsText.NO_FAMILY;

  const family = await notFullFamily.checkFullFamily(message.author, notFullFamily.room_id);
  if (!family) return;

  const sum = parseInt(strSum);
  if (isNaN(sum) || sum <= 0) throw CommandsText.PARAMS_ERROR;

  if (sum < 300) throw CommandsText.TRANSFER_MIN_LOVE_ROOM;

  if (user.coins < sum) {
    const enoughCoins = sum - user.coins;
    const sumRus = noun(sum, 'коин', 'коина', 'коинов');
    const enoughCoinsRus = noun(enoughCoins, 'коин', 'коина', 'коинов');
    const userCoins = noun(user.coins, 'коин', 'коина', 'коинов');

    throw new MessageEmbed()
      .setColor('RED')
      .setTitle(`Недостаточно коинов`)
      .addField('Не хватает', enoughCoins.toFixed(1) + ' ' + enoughCoinsRus, true)
      .addField('Сумма перевода', sum + ' ' + sumRus, true)
      .setFooter('Баланс: ' + user.coins + ' ' + userCoins);
  }

  await user.takeCoins(sum);
  family.balance += sum;
  await family.save();

  const sendText = `${message.member} перевел **${sum}** ${noun(
    sum,
    'коин',
    'коина',
    'коинов',
  )} на счет семьи.`;

  const sendTextLog = `${message.member} перевел **${sum}** ${noun(
    sum,
    'коин',
    'коина',
    'коинов',
  )} на счет семьи **${message.guild!.channels.cache.get(family.room_id)}**.`;

  sendLoveLog(sendTextLog);
  family.sendToFamilyParents(sendText);
};
