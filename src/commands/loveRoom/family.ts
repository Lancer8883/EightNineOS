import { Message, MessageEmbed } from 'discord.js';
import { getRepository } from 'typeorm';
import { LoveRoomsEntity } from '../../database/entities/LoveRooms.entity';
import { LoveRoomsChildrenEntity } from '../../database/entities/LoveRoomsChildren.entity';
import moment from 'moment';
import noun from 'plural-ru';
import { CommandsText } from '../../logs/commands/text';

async function getMembersNameById(id: LoveRoomsChildrenEntity[], message: Message) {
  return Promise.all(
    id.map(async (user) => {
      const member = await message.guild?.members.fetch(user.user_id).catch((e) => {
        return;
      });
      if (!member) return;
      return member.user.toString();
    }),
  );
}

export const family = async (message: Message): Promise<void> => {
  const loveRoomRepository = getRepository(LoveRoomsEntity);
  const notFullFamily = await loveRoomRepository
    .createQueryBuilder('family')
    .leftJoinAndSelect('family.children', 'children')
    .where('children.user_id = :id OR family.firstUser_id = :id OR family.secondUser_id = :id', {
      id: message.author.id,
    })
    .getOne();

  if (!notFullFamily) throw CommandsText.NO_FAMILY;

  const fullFamily = await notFullFamily.checkFullFamily(message.author, notFullFamily.room_id);
  if (!fullFamily) return;

  const children = await getMembersNameById(fullFamily.children, message);
  const firstMember = await message.guild?.members.fetch(fullFamily.firstUser_id);
  const secondMember = await message.guild?.members.fetch(fullFamily.secondUser_id);
  if (!firstMember || !secondMember) return;

  const dateNow = moment().subtract(3, 'month');
  const createdAt = moment(fullFamily.childCD);
  const diff = createdAt.diff(dateNow, 'days');
  const diffRus = noun(diff, 'день', 'дня', 'дней');

  const embed = new MessageEmbed()
    .setColor('#2f3136')
    .setTitle(`Информация о семье 💖`)
    .addField('Первый родитель:', firstMember.user.toString(), true)
    .addField('Второй родитель:', secondMember.user.toString(), true)
    .addField('Баланс:', fullFamily.balance.toString(), true)
    .addField('Дети:', children.length != 0 ? children.join(', ') : 'Нету :(')
    .setFooter(
      dateNow > createdAt ? 'Можно заводить детей' : `Детей можно завести через ${diff} ${diffRus}`,
    );

  message.author.send({
    embeds: [embed],
  });
};
