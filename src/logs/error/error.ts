import { client } from '../../client';
import { getConfig } from '../../config/config';
import { MessageEmbed, TextChannel } from 'discord.js';

export const sendErrorLog = (title: string, text: string): void => {
  const embed = new MessageEmbed().setColor('RED').setTitle(title).setDescription(text);

  (client.channels.cache.get(getConfig().channels.text.errorLog) as TextChannel)?.send({
    embeds: [embed],
  });
};
