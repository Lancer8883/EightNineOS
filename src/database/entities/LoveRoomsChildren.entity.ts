import { Entity, getRepository, JoinColumn, ManyToOne, PrimaryColumn } from 'typeorm';
import { LoveRoomsEntity } from './LoveRooms.entity';

@Entity('love_rooms_children')
export class LoveRoomsChildrenEntity {
  @PrimaryColumn('varchar', { length: 19 })
  user_id: string;

  @JoinColumn({ name: 'room_id' })
  @ManyToOne(() => LoveRoomsEntity, (room) => room.children)
  room_id: LoveRoomsEntity;

  static async addUserToFamily(
    user_id: string,
    room_id: LoveRoomsEntity,
  ): Promise<LoveRoomsChildrenEntity> {
    return getRepository(LoveRoomsChildrenEntity).create({
      user_id,
      room_id,
    });
  }

  public save(): Promise<LoveRoomsChildrenEntity> {
    return getRepository(LoveRoomsChildrenEntity).save(this);
  }
}
