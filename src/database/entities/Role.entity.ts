import { Entity, Column, getRepository, PrimaryColumn } from 'typeorm';
import { GuildMember } from 'discord.js';

@Entity('roles')
export class RoleEntity {
  @PrimaryColumn()
  role_id: string;

  @Column('json')
  access_roles: string[];

  @Column('json')
  access_members: string[];

  save(): Promise<this> {
    return getRepository(RoleEntity).save(this);
  }

  public canUse(member: GuildMember | null): boolean {
    if (!member) return false;
    const canByRole = this.access_roles.some((rId) => member.roles.cache.has(rId));
    const canMyId = this.access_members.includes(member.id);
    return canByRole || canMyId;
  }
}
