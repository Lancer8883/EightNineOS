import {
  Entity,
  Column,
  PrimaryColumn,
  getRepository,
  JoinColumn,
  ManyToOne,
  BeforeUpdate,
} from 'typeorm';
import { client } from '../../client';
import { noun } from 'plural-ru';
import { ClansEntity } from './Clans.entity';
import { getConfig } from '../../config/config';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';
import { MessageEmbed } from 'discord.js';

type UserEquipment = {
  1: number;
  2: number;
  3: number;
  4: number;
  5: number;
  6: number;
};

type TopData<T extends keyof UsersEntity> = Pick<UsersEntity, T | 'user_id'>;
type ClanOption<T extends boolean> = T extends true
  ? UsersEntity
  : Omit<UsersEntity, 'clan'> & { clan: number };

@Entity('users')
export class UsersEntity {
  @PrimaryColumn('varchar', { length: 19 })
  user_id!: string;

  @Column('int', { default: 1 })
  level!: number;

  @Column('double', { default: 0 })
  coins!: number;

  @Column({ default: 0 })
  voice_time!: number;

  @Column({ default: 0 })
  voice_time_week!: number;

  @Column({ default: 0 })
  voice_time_mod!: number;

  @JoinColumn({ name: 'clan_id' })
  @ManyToOne(() => ClansEntity, (clan) => clan.members)
  clan!: ClansEntity;

  @Column({ type: 'smallint', default: 0 })
  warns!: number;

  @Column({ default: 0 })
  afk_warns!: number;

  @Column({ default: 0 })
  messages!: number;

  @Column('datetime', { nullable: true })
  png_subscribe_timeout!: Date | null;

  @Column('json')
  equipment!: UserEquipment;

  @Column('datetime', { nullable: true })
  clan_join_timeout!: Date | null;

  getNextLevelPrice(): number {
    return this.level * 2000;
  }

  getEquipment(): number {
    let count = 0;

    Object.values(this.equipment).forEach((a) => {
      count += Number(a);
    });
    return count;
  }

  save(): Promise<UsersEntity> {
    return getRepository(UsersEntity).save(this);
  }

  /**
     this method is updating/saving the user in db
     */
  async takeCoins(amount: number): Promise<void> {
    amount = Math.floor(amount);

    this.coins -= amount;

    await this.save();

    const takenRus = noun(amount, 'коин', 'коина', 'коинов');
    const balanceRus = noun(this.coins, 'коин', 'коина', 'коинов');

    const embedLoss = new MessageEmbed()
      .setColor('BLUE')
      .setTitle('Изменение баланса')
      .addField('Списано', amount + ' ' + takenRus, true)
      .addField('Баланс', this.coins.toFixed(1) + ' ' + balanceRus, true);

    client.users.cache.get(this.user_id)?.send({ embeds: [embedLoss] });
  }

  /**
     this method is updating/saving the user in db
     */
  async giveCoins(amount: number): Promise<void> {
    amount = Math.floor(amount);

    this.coins += amount;
    await this.save();

    const member = await client.users.fetch(this.user_id);
    if (!member) return;

    const givenRus = noun(amount, 'коин', 'коина', 'коинов');
    const balanceRus = noun(this.coins, 'коин', 'коина', 'коинов');

    const embedProfit = new MessageEmbed()
      .setColor('BLUE')
      .setTitle('Изменение баланса')
      .addField('Начислено', amount + ' ' + givenRus, true)
      .addField('Баланс', this.coins.toFixed(1) + ' ' + balanceRus, true);
    await member.send({ embeds: [embedProfit] });
  }

  static async getTopByColumn<T extends keyof UsersEntity>(
    by: T,
    toIgnore: boolean,
  ): Promise<TopData<T>[]> {
    const query = await getRepository(UsersEntity)
      .createQueryBuilder('user')
      .select(['user.user_id', `user.${by}`])
      .addOrderBy(`user.${by}`, 'DESC')
      .take(10)
      .cache(60000);

    if (toIgnore) {
      for (let i = 0; i < getConfig().topIgnoreUsers.length; i++) {
        query.andWhere(`user.user_id != :id${i}`);
        query.setParameter(`id${i}`, getConfig().topIgnoreUsers[i]);
      }
    }

    return query.getMany() as Promise<TopData<T>[]>;
  }

  static async getTop3ByColumn<T extends keyof UsersEntity>(
    by: T,
    toIgnore: boolean,
  ): Promise<TopData<T>[]> {
    const query = await getRepository(UsersEntity)
      .createQueryBuilder('user')
      .select(['user.user_id', `user.${by}`])
      .addOrderBy(`user.${by}`, 'DESC')
      .take(3)
      .cache(60000);

    if (toIgnore) {
      for (let i = 0; i < getConfig().topIgnoreUsers.length; i++) {
        query.andWhere(`user.user_id != :id${i}`);
        query.setParameter(`id${i}`, getConfig().topIgnoreUsers[i]);
      }
    }

    return query.getMany() as Promise<TopData<T>[]>;
  }

  delete(): Promise<DeleteResult> {
    return getRepository(UsersEntity).delete(this.user_id);
  }

  static async getOrCreateUser<T extends boolean>(
    userID: string,
    clan?: T,
  ): Promise<ClanOption<T>> {
    const repository = getRepository(UsersEntity);
    const user = await (clan
      ? repository.findOne(userID, {
          relations: ['clan'],
        })
      : repository.findOne(userID, { loadRelationIds: true }));

    if (user) return user as ClanOption<T>;

    // @ts-ignore
    const newUser = repository.create({
      user_id: userID,
      level: 1,
      equipment: { '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '6': 0 },
      clan: null,
      clan_join_timeout: null,
    });

    return repository.save(newUser) as unknown as Promise<ClanOption<T>>;
  }

  @BeforeUpdate()
  updateCoins(): void {
    this.coins = parseFloat(this.coins.toFixed(2));
  }
}
