import { Entity, Column, PrimaryColumn, getRepository } from 'typeorm';

@Entity('role_boost')
export class RoleBoostEntity {
  @PrimaryColumn({
    type: 'varchar',
    length: 180,
  })
  role_id!: string;

  @Column('float')
  to_boost!: number;

  save(): Promise<this> {
    return getRepository(RoleBoostEntity).save(this);
  }
}
