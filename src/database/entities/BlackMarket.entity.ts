import { Column, Entity, getRepository, PrimaryColumn } from 'typeorm';

@Entity('black_market')
export class BlackMarketEntity {
  @PrimaryColumn('varchar', { length: 19 })
  message_id!: string;

  @Column('varchar', { length: 19 })
  user_id!: string;

  @Column('tinyint', { default: false })
  decided!: boolean;

  save(): Promise<BlackMarketEntity> {
    return getRepository(BlackMarketEntity).save(this);
  }
}
