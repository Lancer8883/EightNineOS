import { Entity, PrimaryGeneratedColumn, Column, OneToMany, getRepository } from 'typeorm';
import { UsersEntity } from './Users.entity';
import { client } from '../../client';
import { TerritoryEntity } from './Territory.entity';
import { noun } from 'plural-ru';
import { Guild } from 'discord.js';
import { getConfig } from '../../config/config';

@Entity('clans')
export class ClansEntity {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name!: string;

  @Column({ name: 'leader_id', length: 19 })
  leader!: string;

  @Column({ name: 'deputy_id', length: 19, nullable: true, default: null })
  deputy!: string;

  @Column({ type: 'float', default: 0 })
  coins!: number;

  @Column({ default: null })
  room!: string;

  @Column({ default: null })
  chatRoom!: string;

  @Column({ default: 1 })
  level!: number;

  @Column('text', { default: null })
  emblem!: string;

  @Column()
  role!: string;

  @Column('date')
  date!: Date;

  @Column('datetime', { nullable: true, default: null })
  attack_timeout!: Date;

  @Column('datetime', { nullable: true })
  defend_for_attack!: Date;

  @OneToMany(() => UsersEntity, (user) => user.clan)
  members!: UsersEntity[];

  @OneToMany(() => TerritoryEntity, (territory) => territory.owner)
  territories!: TerritoryEntity[];

  public save(): Promise<ClansEntity> {
    return getRepository(ClansEntity).save(this);
  }

  public isClanLeaderOrDeputy(user: UsersEntity): boolean {
    return this.leader === user.user_id || this.deputy === user.user_id;
  }

  public async countPower(): Promise<number> {
    const members = await getRepository(UsersEntity).find({ where: { clan: this.id } });

    return members.reduce((count: number, user: UsersEntity) => {
      let equipCount = 0;
      for (const equip of Object.values(user.equipment)) {
        equipCount += equip;
      }

      count += equipCount;

      return count;
    }, 0);
  }

  public async countMembers(): Promise<number> {
    return getRepository(UsersEntity).count({ where: { clan: this.id } });
  }

  public async getMembers(): Promise<UsersEntity[]> {
    return getRepository(UsersEntity).find({ where: { clan: this.id } });
  }

  public async sendMessageToOwnerAndDeputy(message: string): Promise<void> {
    const owner = await client.users.fetch(this.leader);
    owner?.send(message);

    if (this.deputy) {
      const deputy = await client.users.fetch(this.deputy);
      deputy?.send(message);
    }
  }

  public static getClan(id: number): Promise<ClansEntity | undefined> {
    return getRepository(ClansEntity).findOne(id);
  }

  /**
   this method is updating/saving the clan in db

   */

  async takeCoins(amount: number): Promise<void> {
    this.coins -= Math.floor(amount);
    await this.save();

    const takenRus = noun(amount, 'коин', 'коина', 'коинов');
    const balanceRus = noun(this.coins, 'коин', 'коина', 'коинов');

    this.sendMessageToOwnerAndDeputy(
      `С счёта клана было списано **${amount} ${takenRus} **\nБаланс: **${this.coins}** ${balanceRus}`,
    );
  }

  /**
   this method is updating/saving the clan in db

   */
  async giveCoins(amount: number): Promise<void> {
    this.coins += Math.floor(amount);
    await this.save();

    const givenRus = noun(amount, 'коин', 'коина', 'коинов');
    const balanceRus = noun(this.coins, 'коин', 'коина', 'коинов');

    this.sendMessageToOwnerAndDeputy(
      `На счёт клана было начислено **${amount} ${givenRus} **\nБаланс: **${this.coins}** ${balanceRus}`,
    );
  }

  clanChannel(guild: Guild, toCreate: boolean): void {
    if (toCreate && !this.room) {
      guild.channels
        .create(`Клан ${this.name}`, {
          type: 'GUILD_VOICE',
          parent: getConfig().channels.voice.clanCategory,
          permissionOverwrites: [
            {
              id: this.leader,
              allow: [
                'MANAGE_CHANNELS',
                'MANAGE_ROLES',
                'MOVE_MEMBERS',
                'DEAFEN_MEMBERS',
                'MUTE_MEMBERS',
                'SPEAK',
                'CONNECT',
                'VIEW_CHANNEL',
              ],
            },
            {
              id: guild.id,
              deny: ['CONNECT'],
            },
            {
              id: this.role,
              allow: ['VIEW_CHANNEL', 'CONNECT', 'SPEAK'],
            },
            {
              id: getConfig().roles.moderator,
              allow: ['VIEW_CHANNEL', 'CONNECT', 'SPEAK', 'MOVE_MEMBERS'],
            },
            {
              id: getConfig().roles.mute,
              deny: ['VIEW_CHANNEL', 'CONNECT'],
            },
          ],
        })
        .then((voice) => {
          this.room = voice.id;
          this.save();
        });
    } else if (!toCreate && this.room) {
      const channel = guild.channels.cache.get(this.room);
      if (channel) {
        // @ts-ignore
        this.room = null;
        channel.delete().catch(() => {});

        this.save();
      }
    }
  }

  clanChannelChat(guild: Guild, toCreate: boolean): void {
    if (toCreate && !this.chatRoom) {
      guild.channels
        .create(this.name, {
          type: 'GUILD_TEXT',
          parent: getConfig().channels.voice.clanCategory,
          permissionOverwrites: [
            {
              id: this.leader,
              allow: ['MANAGE_CHANNELS', 'VIEW_CHANNEL', 'MANAGE_MESSAGES'],
            },
            {
              id: guild.id,
              deny: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
            },
            {
              id: this.role,
              allow: ['VIEW_CHANNEL', 'SEND_MESSAGES', 'ATTACH_FILES'],
            },
            {
              id: getConfig().roles.moderator,
              allow: ['MANAGE_CHANNELS', 'MANAGE_ROLES', 'VIEW_CHANNEL', 'MANAGE_MESSAGES'],
            },
            {
              id: getConfig().roles.mute,
              deny: ['VIEW_CHANNEL', 'SEND_MESSAGES'],
            },
          ],
        })
        .then((chat) => {
          this.chatRoom = chat.id;
          this.save();
        });
    } else if (!toCreate && this.chatRoom) {
      const chat = guild.channels.cache.get(this.chatRoom);
      if (chat) {
        // @ts-ignore
        this.chatRoom = null;
        chat.delete().catch(() => {});

        this.save();
      }
    }
  }

  async deleteClan(): Promise<void> {
    const guild = client.guilds.cache.get(getConfig().guild);
    if (!guild) return;

    const role = await guild.roles.fetch(this.role);

    const leader = await getRepository(UsersEntity).findOne(this.leader);
    if (this.coins !== 0) leader?.giveCoins(this.coins);

    role?.delete();
    this.clanChannel(guild, false);
    this.clanChannelChat(guild, false);

    const userRepository = getRepository(UsersEntity);
    const territoryRepository = getRepository(TerritoryEntity);
    const clanRepository = getRepository(ClansEntity);

    await userRepository
      .createQueryBuilder('user')
      .update()
      // @ts-ignore
      .set({ clan: null })
      .where('clan_id = :id', { id: this.id })
      .execute();

    await territoryRepository
      .createQueryBuilder('ter')
      .update()
      // @ts-ignore
      .set({ owner: null })
      .where('owner = :id', { id: this.id })
      .execute();

    await clanRepository.delete(this);
  }
}
