import { Entity, Column, PrimaryColumn, getRepository } from 'typeorm';
import { User } from 'discord.js';

type AvailableStat = Pick<ReddcEntity, 'reports' | 'week_reports' | 'mutes' | 'warns' | 'bans'>;

@Entity('reddc')
export class ReddcEntity {
  @PrimaryColumn('varchar', { length: 19 })
  user_id: string;

  @Column()
  reports: number;

  @Column()
  week_reports: number;

  @Column()
  mutes: number;

  @Column()
  warns: number;

  @Column()
  bans: number;

  save(): Promise<this> {
    return getRepository(ReddcEntity).save(this);
  }

  static async getOrCreateAdmin(userID: string): Promise<ReddcEntity> {
    const repository = getRepository(ReddcEntity);
    const admin = await repository.findOne(userID);

    if (admin) return admin;

    const newAdmin = repository.create({
      user_id: userID,
      reports: 0,
      week_reports: 0,
      bans: 0,
      warns: 0,
      mutes: 0,
    });

    return repository.save(newAdmin);
  }

  static async addStat(user: User, toAddStat: Partial<AvailableStat>): Promise<void> {
    const admin = await ReddcEntity.getOrCreateAdmin(user.id);

    for (const key in toAddStat) {
      admin[key] += toAddStat[key];
    }

    admin.save();
  }
}
