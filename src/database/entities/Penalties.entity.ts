import { Entity, Column, PrimaryColumn, getRepository } from 'typeorm';

@Entity('penalties')
export class PenaltiesEntity {
  @PrimaryColumn('varchar', { length: 19 })
  user_id: string;

  @Column('datetime')
  date: Date;

  @Column('boolean', { default: true })
  isMuted: boolean;

  static async getPenalty(userID: string, isMuted: boolean): Promise<undefined | PenaltiesEntity> {
    return getRepository(PenaltiesEntity).findOne(userID, { where: { isMuted: isMuted } });
  }

  save(): Promise<this> {
    return getRepository(PenaltiesEntity).save(this);
  }
}
