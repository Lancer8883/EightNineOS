import { Entity, PrimaryGeneratedColumn, Column, getRepository } from 'typeorm';

@Entity('price_list')
export class PricelistEntity {
  @PrimaryGeneratedColumn()
  equipment_id!: number;

  @Column('text')
  equipment_name!: string;

  @Column()
  start_price!: number;

  static getPrice(level: number): Promise<PricelistEntity | undefined> {
    return getRepository(PricelistEntity).findOne(level);
  }
}
