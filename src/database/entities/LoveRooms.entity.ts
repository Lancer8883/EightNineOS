import { Column, Entity, getRepository, OneToMany, PrimaryColumn } from 'typeorm';
import { LoveRoomsChildrenEntity } from './LoveRoomsChildren.entity';
import moment from 'moment';
import { client } from '../../client';
import { getConfig } from '../../config/config';
import { MessageEmbed, User } from 'discord.js';

@Entity('love_rooms')
export class LoveRoomsEntity {
  @PrimaryColumn('varchar', { length: 19 })
  room_id: string;

  @Column('varchar', { length: 19 })
  firstUser_id: string;
  @Column('varchar', { length: 19 })
  secondUser_id: string;

  @OneToMany(() => LoveRoomsChildrenEntity, (user) => user.room_id)
  children: LoveRoomsChildrenEntity[];

  @Column('date')
  childCD: Date;

  @Column({ type: 'date', default: moment().format('YYYY-MM-DD') })
  last_payment_date: Date;

  @Column('int', { default: 0 })
  balance: number;

  public save(): Promise<LoveRoomsEntity> {
    return getRepository(LoveRoomsEntity).save(this);
  }

  async takeCoins(): Promise<void> {
    this.balance -= getConfig().loveRoomExtendPrice;
    this.last_payment_date = moment().toDate();

    await this.save();
  }

  sendToFamilyParents(text: string): void {
    const guild = client.guilds.cache.get(getConfig().guild)!;

    const firstMember = guild.members.cache.get(this.firstUser_id);
    const secondMember = guild.members.cache.get(this.secondUser_id);

    if (firstMember) {
      firstMember.send(text);
    }

    if (secondMember) {
      secondMember.send(text);
    }
  }

  async sendToAllMembers(text: MessageEmbed): Promise<void> {
    const guild = client.guilds.cache.get(getConfig().guild)!;

    const firstMember = guild.members.cache.get(this.firstUser_id);
    const secondMember = guild.members.cache.get(this.secondUser_id);

    if (firstMember) {
      firstMember.send({ embeds: [text] });
    }

    if (secondMember) {
      secondMember.send({ embeds: [text] });
    }

    if (this.children) {
      for (const user of this.children) {
        const member = await guild.members.fetch(user.user_id);

        if (member) {
          member.send({ embeds: [text] });
        }
      }
    }
  }

  async deleteRoom(): Promise<void> {
    const guild = client.guilds.cache.get(getConfig().guild);
    if (!guild) return;

    const room = guild.channels.cache.get(this.room_id)!;

    await room.delete();

    const roomRepository = getRepository(LoveRoomsEntity);
    await roomRepository.delete(this.room_id);
  }

  async checkFullFamily(author: User, roomId: string): Promise<LoveRoomsEntity | void> {
    const loveRoomRepository = getRepository(LoveRoomsEntity);

    const family = await loveRoomRepository
      .createQueryBuilder('family')
      .leftJoinAndSelect('family.children', 'children')
      .where('family.room_id = :id', {
        id: roomId,
      })
      .getOne();
    if (!family) return;

    return family;
  }

  async addChildren(user_id: string): Promise<void> {
    const childrens =
      this.children.find((t) => t.user_id === user_id) ||
      (await LoveRoomsChildrenEntity.addUserToFamily(user_id, this));
    this.children = [...this.children.filter((t) => t.user_id !== user_id), childrens];
    await getRepository(LoveRoomsChildrenEntity).save(childrens);
  }
}
