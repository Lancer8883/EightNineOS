import { createConnection } from 'typeorm';
import { PricelistEntity } from './entities/Pricelist.entity';
import { RoleBoostEntity } from './entities/RoleBoost.entity';
import { TerritoryEntity } from './entities/Territory.entity';
import { ClansEntity } from './entities/Clans.entity';
import { PenaltiesEntity } from './entities/Penalties.entity';
import { ReddcEntity } from './entities/Reddc.entity';
import { ShopEntity } from './entities/Shop.entity';
import { UsersEntity } from './entities/Users.entity';
import { MessagesEntity } from './entities/Messages.entity';
import { LotteryEntity } from './entities/Lottery.entity';
import { UserTicketsEntity } from './entities/UserTickets.entity';
import { RoleEntity } from './entities/Role.entity';
import { BlackMarketEntity } from './entities/BlackMarket.entity';
import { startBot } from '../client';
import { updateDBMessageCron } from '../commands/user/tops/topChat/events';
import { nightCron } from '../nightCron/nightCron';
import { cron } from '../voiceCron/cron';
import { weekCron } from '../weekCron/weekCron';

import '../client/onMemberJoin';
import '../client/onMemberLeft';
import '../client/onMemberBan';
import '../API/API';
import '../logs';
import '../voiceLogs';

import { generatePastasOrder } from '../MessagesPastas/utils';
import { sendPastasCron } from '../MessagesPastas/cron';
import { SettingsEntity } from './entities/Settings.entity';
import { LoveRoomsEntity } from './entities/LoveRooms.entity';
import { LoveRoomsChildrenEntity } from './entities/LoveRoomsChildren.entity';

createConnection({
  type: process.env.MYSQL_TYPE as 'mysql',
  host: process.env.MYSQL_HOST,
  port: 3306,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  charset: process.env.MYSQL_CHARSET,
  synchronize: true,
  logging: false,
  entities: [
    ClansEntity,
    PenaltiesEntity,
    PricelistEntity,
    ReddcEntity,
    RoleBoostEntity,
    ShopEntity,
    TerritoryEntity,
    RoleEntity,
    UsersEntity,
    UserTicketsEntity,
    LotteryEntity,
    MessagesEntity,
    BlackMarketEntity,
    SettingsEntity,
    LoveRoomsEntity,
    LoveRoomsChildrenEntity,
  ],
}).then(() => {
  // eslint-disable-next-line no-console
  console.log('Database ready');

  startBot();

  nightCron.start();
  cron.start();
  weekCron.start();

  generatePastasOrder().then(() => sendPastasCron.start());

  updateDBMessageCron();
});
