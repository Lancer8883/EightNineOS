import { CronJob } from 'cron';
import { client } from '../client';
import { GuildMember, VoiceChannel } from 'discord.js';
import { getRepository } from 'typeorm';
import { UsersEntity } from '../database/entities/Users.entity';
import { RoleBoostEntity } from '../database/entities/RoleBoost.entity';
import { getConfig } from '../config/config';

const voiceCheck = async () => {
  const userRepository = getRepository(UsersEntity);
  const boostRepository = getRepository(RoleBoostEntity);
  const config = getConfig();

  const voices = client.channels.cache.filter(
    (channel) => channel.type === 'GUILD_VOICE' && channel.id !== config.channels.voice.afk,
  );

  const members = voices.reduce((acc: GuildMember[], value: VoiceChannel) => {
    acc.push(...value.members.values());

    return acc;
  }, []);

  const users = await userRepository.findByIds(members.map((member) => member.id));
  const allBoostRoles = await boostRepository.find();

  for (const member of members) {
    let user = users.find((user) => user.user_id === member.id);

    if (!user) {
      // @ts-ignore
      user = userRepository.create({
        warns: 0,
        voice_time: 0,
        user_id: member.id,
        messages: 0,
        level: 1,
        equipment: { '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '6': 0 },
        coins: 0,
        clan: null,
        clan_join_timeout: null,
        voice_time_week: 0,
        voice_time_mod: 0,
        png_subscribe_timeout: null,
      });

      users.push(user);
    }

    if (member.voice.deaf) continue;

    user.voice_time_week += 60;
    user.voice_time += 60;

    let newCoins = 1;

    if (user.level > 1) {
      newCoins += (user.level - 1) * 0.2;
    }

    const roles = allBoostRoles.filter((roleID) => member.roles.cache.get(roleID.role_id));
    // eslint-disable-next-line @typescript-eslint/naming-convention
    for (const { to_boost } of roles) {
      newCoins += to_boost;
    }

    if (member.voice.channel?.parentId === config.channels.voice.modRoomCategory) {
      user.voice_time_mod += 60;
      newCoins += 0.5;
    }

    if (
      member.voice.channel?.parentId === config.channels.voice.film ||
      member.voice.channel?.parentId === config.channels.voice.events
    ) {
      newCoins += 0.5;
    }

    user.coins += newCoins;
  }

  userRepository.save(users);
};

export const cron = new CronJob('* * * * *', voiceCheck);
