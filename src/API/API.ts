import { reqUsers } from '../config';
import { UsersEntity } from '../database/entities/Users.entity';
import { noun } from 'plural-ru';
import { getRepository } from 'typeorm';
import { client } from '../client';
import { sendAPILog } from '../logs/admin/channels';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { MessageEmbed } from 'discord.js';

const app = express();

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
);

app.use(bodyParser.json());
app.listen(3000, function () {
  // eslint-disable-next-line no-console
  console.log('API listening on port 3000!');
});

app.use((req, res, next) => {
  if (reqUsers.findIndex((t) => t.token == req.headers.authorization) == -1)
    return res.status(401).send('Unauthorized');
  next();
});

// TODO: query limit && log

app.post('/givecoins/:id/:sum', async function (req, res) {
  if (!req.params.id || !req.params.sum)
    return res.json({
      success: false,
      message: 'We need more params!!',
    });

  const user = await getRepository(UsersEntity).findOne({ user_id: req.params.id });

  const sum = parseInt(req.params.sum);

  if (!user)
    return res.json({
      success: false,
      message: 'No user',
    });

  user.coins += sum;
  await user.save();

  res.json({
    success: true,
    message: `gived ${sum} coins`,
  });
  const embed = new MessageEmbed().setColor('BLUE').setTitle('Изменение баланса')
    .setDescription(`Вам было начислено **${sum} ${noun(sum, 'коин', 'коина', 'коинов')} **\n
      Баланс: **${user.coins.toFixed(1)}** ${noun(
    +user.coins.toFixed(1),
    'коин',
    'коина',
    'коинов',
  )}`);

  client.users.cache.get(req.params.id)?.send({ embeds: [embed] });

  const reqUser = reqUsers.find((user) => user.token == req.headers.authorization);
  sendAPILog(
    `API ${reqUser!.name} отправил <@${user.user_id}> **${sum}** ${noun(
      sum,
      'коин',
      'коина',
      'коинов',
    )}`,
  );
});

app.get('/user/:id', async (req, res) => {
  if (!req.params.id)
    return res.json({
      success: false,
      message: 'We need more params!!',
    });

  const user = await getRepository(UsersEntity).findOne({ user_id: req.params.id });

  if (!user)
    return res.json({
      success: false,
      message: 'No user in database',
    });

  res.json({
    success: true,
    message: user,
  });
});

app.get('/users', async (req, res) => {
  if (!req.query.ids)
    return res.json({
      success: false,
      message: 'We need more params!!',
    });

  try {
    const users: UsersEntity[] = await getRepository(UsersEntity).findByIds(
      req.query.ids as string[],
    );

    return res.json({
      success: true,
      message: users,
    });
  } catch (e) {
    return res.json({
      success: false,
      message: e.message,
    });
  }
});
