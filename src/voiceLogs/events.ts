import { MessageEmbed, VoiceState } from 'discord.js';
import { client } from '../client';
import { getConfig } from '../config/config';
import { sendVoiceLog } from '../logs/admin/channels';
import moment from 'moment';

client.on('voiceStateUpdate', async (oldVoice: VoiceState, newVoice: VoiceState) => {
  const member = newVoice.channel ? newVoice.member : oldVoice.member;
  if (newVoice.channel === oldVoice.channel) return;

  if (newVoice.member!.roles.cache.has(getConfig().roles.mute))
    return void newVoice.member!.voice.disconnect();

  const date = moment().format('hh:mm:ss DD.MM.YYYY');

  if (member?.voice.channel && oldVoice.channel) {
    sendVoiceLog(
      new MessageEmbed()
        .setDescription(
          `:rocket: Участник ${member} ID: ${member.id}
  переместился из канала <#${oldVoice.channel.id}> в <#${newVoice.channel!.id}> [\`${date}\`]`,
        )
        .setColor('BLUE'),
    );
  } else if (member?.voice.channel) {
    sendVoiceLog(
      new MessageEmbed()
        .setDescription(
          `:door: Участник ${member} ID: ${member.id} 
  вошёл в канал <#${newVoice.channel!.id}> [\`${date}\`]`,
        )
        .setColor('GREEN'),
    );
  } else if (oldVoice.channel)
    sendVoiceLog(
      new MessageEmbed()
        .setDescription(
          `:no_pedestrians: Участник ${member} ID: ${member!.id} вышел из канала <#${
            oldVoice.channel.id
          }> [\`${date}\`]`,
        )
        .setColor('RED'),
    );
});
